﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pulssoft.Arquiz.UI.Game
{
    public class HomeScreen : NavigationScreen
    {
        public event Action OpenCameraClicked;

        [SerializeField]
        private GetMyProfileResultsRequest getMyProfileResultsRequest;

        private int age;

        private void Awake()
        {
            NavigationBar.ProfileSelected += DeactivateScreen;
            NavigationBar.SettingsSelected += DeactivateScreen;

            // TimeSpan birthday = TimeSpan.FromMilliseconds(AccountSingleton.instance.Account.birthday);
            // DateTime startdate = new DateTime(birthday.Ticks);

            // age = DateTime.Now.Year - startdate.Year;
        }

        private void OnEnable()
        {
            getMyProfileResultsRequest.GetMyProfileResultRequestMethod();
            NavigationBar.HomeToggle.isOn = true;

        }

        public void OpenCameraClick()
        {
            SceneManager.LoadScene("ARScene");
        }
    }
}
