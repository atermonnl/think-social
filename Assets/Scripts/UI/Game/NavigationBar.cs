﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Game
{
    public class NavigationBar : MonoBehaviour
    {
        [SerializeField]
        private Toggle homeToggle = null;
        [SerializeField]
        private Toggle profileToggle = null;
        [SerializeField]
        private Toggle settingsToggle = null;

        public event Action HomeSelected;
        public event Action ProfileSelected;
        public event Action SettingsSelected;

        public Toggle HomeToggle => homeToggle;
        public Toggle ProfileToggle => profileToggle;
        public Toggle SettingsToggle => settingsToggle;

        private void Awake()
        {
            homeToggle.onValueChanged.AddListener(HandleHomeToggleCheck);
            profileToggle.onValueChanged.AddListener(HandleProfileToggleCheck);
            settingsToggle.onValueChanged.AddListener(HandleSettingsToggleCheck);
        }

        private void HandleHomeToggleCheck(bool value)
        {
            if (!value)
            {
                return;
            }
            
            HomeSelected?.Invoke();
        }

        private void HandleProfileToggleCheck(bool value)
        {
            if (!value)
            {
                return;
            }
            
            ProfileSelected?.Invoke();
        }

        private void HandleSettingsToggleCheck(bool value)
        {
            if (!value)
            {
                return;
            }
         
            SettingsSelected?.Invoke();
        }
    }
}