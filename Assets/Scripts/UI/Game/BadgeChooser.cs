﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadgeChooser : MonoBehaviour
{
    [SerializeField]
    private GameObject[] badges;

    private void Awake()
    {
        if (BadgeResultSingleton.instance.resultBadge == "gold")
        {
            Activator(true, false, false);
        }
        else if (BadgeResultSingleton.instance.resultBadge == "silver")
        {
            Activator(false, true, false);
        }
        else
        {
            Activator(false, false, true);
        }
    }

    private void Activator(bool gold, bool silver, bool bronze)
    {
        badges[0].SetActive(gold);
        badges[1].SetActive(silver);
        badges[2].SetActive(bronze);
    }
}
