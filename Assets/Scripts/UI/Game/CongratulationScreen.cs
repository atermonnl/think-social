﻿using System;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pulssoft.Arquiz.UI.Game
{
    public class CongratulationScreen : AuthUIScreen
    {
        [SerializeField]
        private ResultScreen resultScreen;

        protected override void Awake()
        {
            base.Awake();

            BackClicked += BackButtonClick;
        }

        public void HandleErasmusstopClick()
        {
            Application.OpenURL(AppConfig.TermsOfUseUrl);
        }

        public void GoToResultScreen()
        {
            this.DeactivateScreen();
            resultScreen.ActivateScreen();
        }

        private void BackButtonClick()
        {
            this.DeactivateScreen();
            SceneManager.LoadScene("Game");
        }

    }
}