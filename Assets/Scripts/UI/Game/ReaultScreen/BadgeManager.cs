﻿using UnityEngine;
using UnityEngine.UI;

public class BadgeManager : MonoBehaviour
{
    [SerializeField]
    private GameObject goldText;
    [SerializeField]
    private GameObject silverText;
    [SerializeField]
    private GameObject bronzeText;


    public void ShowBadge(string badge)
    {
        if (badge == "gold")
        {
            Activator(true, false, false);
        }
        else if (badge == "silver")
        {
            Activator(false, true, false);
        }
        else if (badge == "bronze")
        {
            Activator(false, false, true);
        }
        else 
        {
            Activator(false, false, false);
        }
    }

    private void Activator(bool gold, bool silver, bool bronze)
    {
        goldText.SetActive(gold);
        silverText.SetActive(silver);
        bronzeText.SetActive(bronze);
    }
}
