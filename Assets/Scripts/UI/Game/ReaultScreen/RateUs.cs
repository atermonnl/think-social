﻿using Pulssoft.Arquiz.Configs;
using UnityEngine;
using UnityEngine.UI;

public class RateUs : MonoBehaviour
{
    [SerializeField]
    private Toggle[] stars = new Toggle[5];

    public void Rater()
    {
        for (int indexToggle = 0; indexToggle < 4; indexToggle++)
        {
            if (stars[indexToggle + 1].isOn == true)
            {
                stars[indexToggle].isOn = true;
            }
        }
    }

    public void RateOnMarket3()
    {
        if (stars[3].isOn == true && stars[4].isOn == false)
        {
            Application.OpenURL(AppConfig.RateUsUrl);
        }
    }

    public void RateOnMarket4()
    {
        if (stars[4].isOn == true)
        {
            Application.OpenURL(AppConfig.RateUsUrl);
        }
    }
}
