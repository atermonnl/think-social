﻿using System;
using System.Collections;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Game
{
    public class ResultScreen : AuthUIScreen
    {
        [SerializeField]
        private Text imageScannedText;
        [SerializeField]
        private Text correctAnswersText;
        [SerializeField]
        private Text totalScoreText;
        [SerializeField]
        private Text badgesText;
        [SerializeField]
        private Text timeSpantText;
        [Space]
        [SerializeField]
        private ARSceneManager aRSceneManager;
        [SerializeField]
        private ResultCalculator resultCalculator;
        [SerializeField]
        private BadgeManager badgeManager;

        private int totalScore;
        private string badge;

        public string Badge => badge;

        protected override void Awake()
        {
            base.Awake();

            totalScore = resultCalculator.GetTotalScore(aRSceneManager.CorrectAnswersCount);
            badge = resultCalculator.GetBadges(totalScore);

            imageScannedText.text = aRSceneManager.ImageScannedCount.ToString();
            correctAnswersText.text = aRSceneManager.CorrectAnswersCount.ToString();
            totalScoreText.text = totalScore.ToString();
            badgeManager.ShowBadge(badge);
            timeSpantText.text = aRSceneManager.TimeSpent.ToString();
            BadgeResultSingleton.GetInstance().resultBadge = badge;
        }

        public void BackClick()
        {
            StartCoroutine(resultCalculator.SaveResults(this, true, aRSceneManager.ImageScannedCount, totalScore, badge));
        }

        public void LoadBadgeScene()
        {
            if (badge != "none" && badge != null)
            {
                StartCoroutine(resultCalculator.SaveResults(this, true, aRSceneManager.ImageScannedCount, totalScore, badge));
                SceneManager.LoadScene("ARBadge");
            }
        }

        public void HandleReplayClick()
        {
            this.DeactivateScreen();
            SceneManager.LoadScene("ARScene");
        }
    }
}
