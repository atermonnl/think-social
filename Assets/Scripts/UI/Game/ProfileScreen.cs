﻿using System;
using Pulssoft.Screens;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;

namespace Pulssoft.Arquiz.UI.Game
{
    public class ProfileScreen : NavigationScreen
    {
        public event Action EditClicked;
        public event Action LogoutClicked;

        [SerializeField]
        private TextMeshProUGUI usernameLabel;
        [SerializeField]
        private TextMeshProUGUI emailLabel;

        [SerializeField]
        private Image profileAvatar;
        [SerializeField]
        private AvatarStruct avatarStruct;

        [SerializeField]
        private Text imageScannedLabel;
        [SerializeField]
        private Text totalScoreLabel;
        [SerializeField]
        private Text badgeLabel;

        public TextMeshProUGUI UsernameLabel { get { return usernameLabel; } set { value = usernameLabel; } }
        public TextMeshProUGUI EmailLabel { get { return emailLabel; } set { value = emailLabel; } }

        private void Awake()
        {
            int avatarId = Int32.Parse(AccountSingleton.instance.Account.avatarId);
            profileAvatar.overrideSprite = avatarStruct.avatars[avatarId - 1].avatar;

            usernameLabel.text = AccountSingleton.instance.Account.username;
            emailLabel.text = AccountSingleton.instance.Account.email;

            NavigationBar.HomeSelected += DeactivateScreen;
            NavigationBar.SettingsSelected += DeactivateScreen;
        }

        public void OnEnable()
        {
            NavigationBar.ProfileToggle.isOn = true;
            imageScannedLabel.text = AccountSingleton.instance.Account.imageScanCount.ToString();
            totalScoreLabel.text = AccountSingleton.instance.Account.totalScore.ToString();
            if (AccountSingleton.instance.Account.badge == "none")
            {
                badgeLabel.text = null;
            }
            else
            {
                badgeLabel.text = AccountSingleton.instance.Account.badge;
            }

        }

        public void HandleEditClick()
        {
            EditClicked?.Invoke();
        }

        public void HandleLogoutClick()
        {
            LogoutClicked?.Invoke();
            AccountSingleton.Reset();
            TokenSingleton.Reset();
            IndexContentSingleton.Reset();
            ImagesSingleton.Reset();
            PasswordSingleton.Reset();
        }
    }
}