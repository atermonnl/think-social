﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Game
{
    public class LanguageScreen : AuthUIScreen
    {
        [SerializeField]
        private Toggle englishToggle = null;
        [SerializeField]
        private Toggle croatianToggle = null;
        [SerializeField]
        private Toggle greekToggle = null;
        [SerializeField]
        private Toggle italianToggle = null;
        [SerializeField]
        private Toggle spanishToggle = null;

        [SerializeField]
        private Text englishLabel = null;
        [SerializeField]
        private Text croatianLabel = null;
        [SerializeField]
        private Text greekLabel = null;
        [SerializeField]
        private Text italianLabel = null;
        [SerializeField]
        private Text spanishLabel = null;

        [SerializeField]
        public Font boldFont;
        [SerializeField]
        public Font lightFont;

        public event Action EnglishSelected;
        public event Action CroatianSelected;
        public event Action GreekSelected;
        public event Action ItalianSelected;
        public event Action SpanishSelected;

        protected override void Awake()
        {
            base.Awake();

            BackClicked += DeactivateScreen;

            if (PlayerPrefs.GetString("language") == "ENG")
            {
                englishToggle.isOn = true;
                HandleEnglishToggleCheck(true);
            }
            else if (PlayerPrefs.GetString("language") == "CRO")
            {
                croatianToggle.isOn = true;
                HandleCroatianToggleCheck(true);
            }
            else if (PlayerPrefs.GetString("language") == "GRE")
            {
                greekToggle.isOn = true;
                HandleGreekToggleCheck(true);
            }
            else if (PlayerPrefs.GetString("language") == "ITA")
            {
                italianToggle.isOn = true;
                HandleItalianToggleCheck(true);
            }
            else if (PlayerPrefs.GetString("language") == "SPA")
            {
                spanishToggle.isOn = true;
                HandleSpanishToggleCheck(true);
            }

            englishToggle.onValueChanged.AddListener(HandleEnglishToggleCheck);
            croatianToggle.onValueChanged.AddListener(HandleCroatianToggleCheck);
            greekToggle.onValueChanged.AddListener(HandleGreekToggleCheck);
            italianToggle.onValueChanged.AddListener(HandleItalianToggleCheck);
            spanishToggle.onValueChanged.AddListener(HandleSpanishToggleCheck);
        }

        public void HandleEnglishToggleCheck(bool value)
        {
            if (!value)
            {
                englishLabel.font = lightFont;
            }
            ChangeToggleLabelFont(englishToggle, englishLabel);
            EnglishSelected?.Invoke();
        }

        public void HandleCroatianToggleCheck(bool value)
        {
            if (!value)
            {
                croatianLabel.font = lightFont;
            }
            ChangeToggleLabelFont(croatianToggle, croatianLabel);
            CroatianSelected?.Invoke();
        }

        public void HandleGreekToggleCheck(bool value)
        {
            if (!value)
            {
                greekLabel.font = lightFont;
            }
            ChangeToggleLabelFont(greekToggle, greekLabel);
            GreekSelected?.Invoke();
        }

        public void HandleItalianToggleCheck(bool value)
        {
            if (!value)
            {
                italianLabel.font = lightFont;
            }
            ChangeToggleLabelFont(italianToggle, italianLabel);
            ItalianSelected?.Invoke();
        }

        public void HandleSpanishToggleCheck(bool value)
        {
            if (!value)
            {
                spanishLabel.font = lightFont;
            }
            ChangeToggleLabelFont(spanishToggle, spanishLabel);
            SpanishSelected?.Invoke();
        }

        public void ChangeToggleLabelFont(Toggle toggle, Text toggleLabel)
        {
            if (toggle.isOn == true)
            {
                toggleLabel.font = boldFont;
            }
            else 
            {
                toggleLabel.font = lightFont;
            }
        }
    }
}