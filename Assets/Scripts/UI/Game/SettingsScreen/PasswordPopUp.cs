﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Game
{
    public class PasswordPopUp : AuthUIScreen, IPointerClickHandler
    {
        [SerializeField]
        private GameObject popUp;
        [SerializeField]
        private InputField currentPassword;
        [SerializeField]
        private InputField newPassword;
        [SerializeField]
        private InputField repeatNewPassword;

        public InputField CurrentPassword => currentPassword;
        public InputField NewPassword => newPassword;
        public InputField RepeatNewPassword => repeatNewPassword;

        public event Action OkClicked;

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            if (pointerEventData.pointerCurrentRaycast.gameObject != popUp)
            {
                currentPassword.text = null;
                newPassword.text = null;
                repeatNewPassword.text = null;
                this.DeactivateScreen();
            }
        }

        public void OkClick()
        {
            OkClicked?.Invoke();
        }
    }
}
