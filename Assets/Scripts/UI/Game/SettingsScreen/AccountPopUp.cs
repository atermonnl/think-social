﻿using Pulssoft.Screens;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Game
{
    public class AccountPopUp : AuthUIScreen, IPointerClickHandler
    {
        [SerializeField]
        private AccountPopUp accountPopUp;
        [SerializeField]
        private GameObject popUp;
        [SerializeField]
        private InputField passwordInputField;

        public Text tipLabel;

        public InputField PasswordInputField => passwordInputField;

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            if (pointerEventData.pointerCurrentRaycast.gameObject != popUp)
            {
                this.accountPopUp.DeactivateScreen();
            }
        }
    }
}