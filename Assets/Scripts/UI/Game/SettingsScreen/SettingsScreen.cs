﻿using System;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Screens;
using UnityEngine;

namespace Pulssoft.Arquiz.UI.Game
{
    public class SettingsScreen : NavigationScreen
    {
        public event Action ChangePasswordClicked;
        public event Action DeleteAccountClicked;
        public event Action ChangeLanguageClicked;

        private void Awake()
        {
            NavigationBar.HomeSelected += DeactivateScreen;
            NavigationBar.ProfileSelected += DeactivateScreen;
            ChangeLanguageClicked += DeactivateScreen;
        }

        private void OnEnable()
        {
            NavigationBar.SettingsToggle.isOn = true;
        }

        public void ChangePasswordClick()
        {
            ChangePasswordClicked?.Invoke();
        }

        public void DeleteAccountClick()
        {
            DeleteAccountClicked?.Invoke();
        }

        public void ChangeLanguageClick()
        {
            ChangeLanguageClicked?.Invoke();
        }

        public void HandlePrivacyPolicyClick()
        {
            Application.OpenURL(AppConfig.PrivacyPolicyUrl);
        }

        public void HandleTermsOfUseClick()
        {
            Application.OpenURL(AppConfig.TermsOfUseUrl);
        }
    }
}