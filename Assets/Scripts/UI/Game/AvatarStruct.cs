﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class AvatarStruct : MonoBehaviour
{
    [Serializable]
    public struct Avatars
    {
        public int id;
        public Sprite avatar;
    }

    public Avatars[] avatars;
  
}
