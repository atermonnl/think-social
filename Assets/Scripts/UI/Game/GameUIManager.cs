﻿using Pulssoft.Screens;
using UnityEngine;

namespace Pulssoft.Arquiz.UI.Game
{
    public class GameUIManager : MonoBehaviour
    {
        [SerializeField]
        private HomeScreen homeScreen = null;
        [SerializeField]
        private ProfileScreen profileScreen = null;
        [SerializeField]
        private SettingsScreen settingsScreen = null;
        [SerializeField]
        private LanguageScreen languageScreen = null;
        [SerializeField]
        private PasswordPopUp passwordPopUp = null;
        [SerializeField]
        private AccountPopUp accountPopUp = null;
        [SerializeField]
        private EditProfilePopUp editProfilePopUp = null;

        public HomeScreen HomeScreen => homeScreen;
        public ProfileScreen ProfileScreen => profileScreen;
        public SettingsScreen SettingsScreen => settingsScreen;

        private void Awake()
        {
            #region NavigationBar

            homeScreen.NavigationBar.ProfileSelected += profileScreen.ActivateScreen;
            homeScreen.NavigationBar.SettingsSelected += settingsScreen.ActivateScreen;

            profileScreen.NavigationBar.HomeSelected += homeScreen.ActivateScreen;
            profileScreen.NavigationBar.SettingsSelected += settingsScreen.ActivateScreen;

            settingsScreen.NavigationBar.HomeSelected += homeScreen.ActivateScreen;
            settingsScreen.NavigationBar.ProfileSelected += profileScreen.ActivateScreen;
            #endregion

            #region SettingsScreen

            settingsScreen.ChangePasswordClicked += passwordPopUp.ActivateScreen;
            settingsScreen.DeleteAccountClicked += accountPopUp.ActivateScreen;

            passwordPopUp.OkClicked += passwordPopUp.DeactivateScreen;

            settingsScreen.ChangeLanguageClicked += languageScreen.ActivateScreen;

            #endregion

            #region LanguageScreen

            languageScreen.BackClicked += settingsScreen.ActivateScreen;

            #endregion

            profileScreen.EditClicked += editProfilePopUp.ActivateScreen;
            
        }

        private void OnEnable()
        {
            DeepLinkManager.Instance.activeFromDeeplink = false;
        }
    }
}