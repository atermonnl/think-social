﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Game
{
    public class EditProfilePopUp : AuthUIScreen, IPointerClickHandler
    {
        [SerializeField]
        private GameObject popUp;
        [SerializeField]
        private InputField usernameInputField;
        [SerializeField]
        private InputField emailInputField;

        public InputField UsernameInputField => usernameInputField;
        public InputField EmailInputField => emailInputField;

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            if (pointerEventData.pointerCurrentRaycast.gameObject != popUp)
            {
                this.DeactivateScreen();
            }
        }
    }
}