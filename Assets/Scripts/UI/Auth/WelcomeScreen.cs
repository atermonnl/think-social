﻿using System;
using Pulssoft.Arquiz.Configs;
using UnityEngine;

namespace Pulssoft.Arquiz.UI.Auth
{
    public class WelcomeScreen : Pulssoft.Screens.Screen
    {
        public event Action LoginClicked;
        public event Action SignUpClicked;

        private void Awake()
        {
            LoginClicked += DeactivateScreen;
            SignUpClicked += DeactivateScreen;
        }

        public void HandleLoginClick()
        {
            LoginClicked?.Invoke();
        }

        public void HandleSignUpClick()
        {
            SignUpClicked?.Invoke();
        }

        public void OpenPrivacyPolicy()
        {
            Application.OpenURL(AppConfig.PrivacyPolicyUrl);
        }
    }
}