﻿using UnityEngine;
using UnityEngine.UI;

public class AvatarId : MonoBehaviour
{
    public Toggle avatarToggle;
    public string avatarId;
    public Image avatarImage;
}
