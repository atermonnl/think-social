﻿using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Auth
{
    public class NewPasswordScreen : AuthUIScreen
    {
        [SerializeField] private InputField newPassword;
        [SerializeField] private AuthErrorPopUp popUpMessage = null;

        public InputField NewPassword => newPassword;
        public AuthErrorPopUp AuthErrorPopUp
        {
            get { return popUpMessage; }
            set { value = popUpMessage; }
        }
    }
}