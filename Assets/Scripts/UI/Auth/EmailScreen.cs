﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz
{
    public class EmailScreen : AuthUIScreen
    {
        [Serializable]
        public class SignInViewModel
        {
            public string email;
            public string password;
        }

        [SerializeField]
        private InputField emailInputField;

        public Account account { get; private set; }

        public event Action SignUpClicked;


        protected override void Awake()
        {
            base.Awake();

            account = new Account();

            SignUpClicked += DeactivateScreen;
        }

        public SignInViewModel mapToSignInViewModel(Account account)
        {
            return new SignInViewModel
            {
                email = account.email,
                password = account.password,
            };
        }

        public void HandleSignUpClick()
        {
            SignUpClicked?.Invoke();
        }

        public void InitAccountModel()
        {
            account.email = emailInputField.text;
        }
    }
}