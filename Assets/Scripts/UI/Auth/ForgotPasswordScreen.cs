﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Auth
{
    public class ForgotPasswordScreen : AuthUIScreen
    {
        [SerializeField]
        private AuthErrorPopUp popUpMessage = null;
        [SerializeField]
        private InputField emailInputField = null;

        public InputField EmailInputField => emailInputField;

        public AuthErrorPopUp AuthErrorPopUp
        {
            get { return popUpMessage; }
            set { value = popUpMessage; }
        }

        public event Action SendClicked;

        protected override void Awake()
        {
            BackClicked += DeactivateScreen;
        }

        public void HandleSendClick()
        {
            SendClicked?.Invoke();
        }
    }
}