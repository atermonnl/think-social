﻿using UnityEngine;

namespace Pulssoft.Arquiz.UI.Auth
{
    public class AuthUIManager : MonoBehaviour
    {
        [SerializeField]
        private WelcomeScreen welcomeScreen = null;
        [SerializeField]
        private EmailScreen emailScreen = null;
        [SerializeField]
        private PasswordScreen passwordScreen = null;
        [SerializeField]
        private UserInfoScreen userInfoScreen = null;
        [SerializeField]
        private AvatarScreen avatarScreen = null;
        [SerializeField]
        private ForgotPasswordScreen forgotPasswordScreen = null;

        public PasswordScreen PasswordScreen => passwordScreen;
        public AvatarScreen AvatarScreen => avatarScreen;

        private void Awake()
        {
            welcomeScreen.LoginClicked += emailScreen.ActivateScreen;
            welcomeScreen.SignUpClicked += userInfoScreen.ActivateScreen;

            emailScreen.BackClicked += welcomeScreen.ActivateScreen;
            emailScreen.NextClicked += passwordScreen.ActivateScreen;
            emailScreen.SignUpClicked += userInfoScreen.ActivateScreen;
            
            passwordScreen.BackClicked += emailScreen.ActivateScreen;
            passwordScreen.ForgotPasswordClicked += forgotPasswordScreen.ActivateScreen;

            userInfoScreen.BackClicked += welcomeScreen.ActivateScreen;

            avatarScreen.BackClicked += userInfoScreen.ActivateScreen;

            forgotPasswordScreen.BackClicked += passwordScreen.ActivateScreen;
        }
    }
}