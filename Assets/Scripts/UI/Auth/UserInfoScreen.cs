﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Auth
{
    public class UserInfoScreen : AuthUIScreen
    {
        [Serializable]
        public class SignUpViewModel
        {
            public string username;
            public string email;
            public string password;
            public string avatarId;
        }

        public Account account = new Account();

        [SerializeField]
        private InputField username;
        [SerializeField]
        private InputField email;
        [SerializeField]
        private InputField password;
       
        [SerializeField]
        AvatarScreen avatarScreen;

        [SerializeField]
        GameObject emptyFieldsPopUp;

        private int prevLength;
        private TimeSpan ageConvert;

        public SignUpViewModel mapToSignUpViewModel(Account account)
        {
            return new SignUpViewModel
            {
                username = account.username,
                email = account.email,
                password = account.password,
                avatarId = account.avatarId,
            };
        }

        public event Action SignUpClicked;

        protected override void Awake()
        {
            BackClicked += DeactivateScreen;
        }

        void Start()
        {
            // age.onValueChanged.AddListener(OnValueChanged);
        }

        // public void OnValueChanged(string str)
        // {
        //     if (str.Length > 0)
        //     {
        //         age.onValueChanged.RemoveAllListeners();
        //         if (!char.IsDigit(str[str.Length - 1]) && str[str.Length - 1] != '/')
        //         { // Remove Letters
        //             age.text = str.Remove(str.Length - 1);
        //             age.caretPosition = age.text.Length;
        //         }
        //         else
        //         {
        //             DayOfBirthValidation(str, age);

        //             if (str.Length > 3)
        //             {
        //                 MounthOfBirthValidation(str, age);
        //             }
        //             if (str.Length == 10)
        //             {
        //                 YearOfBirthValidation(str, age);
        //             }

        //             if (str.Length == 2 || str.Length == 5)
        //             {
        //                 if (str.Length < prevLength)
        //                 { // Delete
        //                     age.text = str.Remove(str.Length - 1);
        //                     age.caretPosition = age.text.Length;
        //                 }
        //                 else
        //                 { // Add
        //                     age.text = str + "/";
        //                     age.caretPosition = age.text.Length;
        //                 }
        //             }
        //         }

        //         age.onValueChanged.AddListener(OnValueChanged);
        //     }

        //     prevLength = age.text.Length;
        // }

        // private void DayOfBirthValidation(string str, InputField age)
        // {
        //     if (str.ToCharArray()[0] == '0')
        //     {
        //         age.text = str;
        //         age.caretPosition = age.text.Length;

        //         if (str.Length > 1 && str.ToCharArray()[1] == '0')
        //         {
        //             age.text = str.Remove(str.Length - 1);
        //             age.caretPosition = age.text.Length;
        //         }
        //         else
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //     }
        //     else if (str.ToCharArray()[0] == '1')
        //     {
        //         age.text = str;
        //         age.caretPosition = age.text.Length;
        //     }
        //     else if (str.ToCharArray()[0] == '2')
        //     {
        //         age.text = str;
        //         age.caretPosition = age.text.Length;
        //     }
        //     else if (str.ToCharArray()[0] == '3')
        //     {
        //         age.text = str;
        //         age.caretPosition = age.text.Length;

        //         if (str.Length > 1 && str.ToCharArray()[1] == '0')
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //         else if (str.Length > 1 && str.ToCharArray()[1] == '1')
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //         else if (str.Length > 1)
        //         {
        //             age.text = str.Remove(str.Length - 1);
        //             age.caretPosition = age.text.Length;
        //         }
        //     }
        //     else
        //     {
        //         age.text = null;
        //         age.caretPosition = age.text.Length;
        //     }
        // }

        // private void MounthOfBirthValidation(string str, InputField age)
        // {
        //     if (str.Length > 3 && str.ToCharArray()[3] == '0')
        //     {
        //         age.text = str;
        //         age.caretPosition = age.text.Length;

        //         if (str.Length > 4 && str.ToCharArray()[4] == '0')
        //         {
        //             age.text = str.Remove(str.Length - 1);
        //             age.caretPosition = age.text.Length;
        //         }
        //         else
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //     }
        //     else if (str.Length > 3 && str.ToCharArray()[3] == '1')
        //     {
        //         age.text = str;
        //         age.caretPosition = age.text.Length;

        //         if (str.Length > 4 && str.ToCharArray()[4] == '0')
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //         else if (str.Length > 4 && str.ToCharArray()[4] == '1')
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //         else if (str.Length > 4 && str.ToCharArray()[4] == '2')
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //         else if (str.Length > 4)
        //         {
        //             age.text = str.Remove(str.Length - 1);
        //             age.caretPosition = age.text.Length;
        //         }
        //     }
        //     else
        //     {
        //         age.text = str.Remove(str.Length - 1);
        //         age.caretPosition = age.text.Length;
        //     }
        // }

        // private void YearOfBirthValidation(string str, InputField age)
        // {
        //     int yearOfBirth =  Int32.Parse(str.Substring(6));

        //     int correctAge = DateTime.Now.Year - yearOfBirth;

        //     string withoutDay = str.Substring(3);
        //     string mounth = withoutDay.Substring(0, withoutDay.Length - 5);

        //     if (mounth == "02")
        //     {
        //         if (str.ToCharArray()[0] == '0')
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //         else if (str.ToCharArray()[0] == '1')
        //         {
        //             age.text = str;
        //             age.caretPosition = age.text.Length;
        //         }
        //         else if (str.ToCharArray()[0] == '2')
        //         {
        //             if (yearOfBirth % 4 == 0)
        //             {
        //                 age.text = str;
        //                 age.caretPosition = age.text.Length;
        //             }
        //             else if (str.ToCharArray()[1] == '9')
        //             {
        //                 age.text = null;
        //                 age.caretPosition = age.text.Length;
        //             }
        //         }
        //         else
        //         {
        //             age.text = null;
        //             age.caretPosition = age.text.Length;
        //         }
        //     }
        // }

        private void InitAccountModel()
        {
            // DateTime birthday = new DateTime(Int32.Parse(age.text.Substring(6)), Int32.Parse(age.text.Substring(3, age.text.Length - 8)),
            //     Int32.Parse(age.text.Substring(0, age.text.Length - 8)));
            // ageConvert = birthday - DateTime.MinValue;

            account.username = username.text;
            account.email = email.text;
            account.password = password.text;
        
        }

        public void GoToAvatarScreen()
        {
            if (username.text == "" || email.text == "" || password.text == "" || email.text.Length <= 6)
            {
                emptyFieldsPopUp.SetActive(true);
            }
            else
            {
                InitAccountModel();
                this.DeactivateScreen();
                avatarScreen.ActivateScreen();
            }
        }
    }
}
