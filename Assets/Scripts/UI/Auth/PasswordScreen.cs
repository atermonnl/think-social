﻿using System;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Auth
{
    public class PasswordScreen : AuthUIScreen
    {
        [SerializeField]
        private EmailScreen emailScreen;

        [SerializeField]
        private InputField passwordInputField; 

        public event Action ForgotPasswordClicked;

        protected override void Awake()
        {
            BackClicked += DeactivateScreen;

            ForgotPasswordClicked += DeactivateScreen;
        }

        public void HandleForgotPasswordClick()
        {
            ForgotPasswordClicked?.Invoke();
        }

        public void InitAccountModel()
        {
            emailScreen.account.password = passwordInputField.text;
            AccountSingleton.GetInstance().Account = emailScreen.account;
        }
    }
}