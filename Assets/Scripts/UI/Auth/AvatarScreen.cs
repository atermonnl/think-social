﻿using System.Collections;
using System.Linq;
using System.Text;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Auth;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Auth
{
    public class AvatarScreen : AuthUIScreen
    {
        [SerializeField]
        private Button signUpButton = null;
        [SerializeField]
        private Toggle privacyPolicyToggle = null;
        [SerializeField]
        private UserInfoScreen userInfoScreen = null;

        [SerializeField]
        private ToggleGroup avatarsToggleGroup;

        protected override void Awake()
        {
            base.Awake();

            privacyPolicyToggle.onValueChanged.AddListener(ToggleSignUpInteractivity);
        }

        private void ToggleSignUpInteractivity(bool interctable)
        {
            signUpButton.interactable = interctable;
        }
        
        public void AvatarChoose()
        {
            Toggle selectedToggle = avatarsToggleGroup.ActiveToggles().FirstOrDefault();
            AvatarId avatarIdSelectedToggle = selectedToggle.GetComponent<AvatarId>();
            userInfoScreen.account.avatarId = avatarIdSelectedToggle.avatarId.ToString();
            AccountSingleton.GetInstance().Account = userInfoScreen.account;
        }
    }
}