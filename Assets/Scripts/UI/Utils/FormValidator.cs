﻿using System.Collections;
using System.Collections.Generic;
using Pulssoft.Auth;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Utils
{
    public class FormValidator : MonoBehaviour
    {
        [SerializeField]
        private Text errorLabel;
        [SerializeField]
        private FieldValidator[] fields;

        public bool ValidateAll()
        {
            foreach (var field in fields)
            {
                string err = field.Validate();
                if (err != null)
                {
                    errorLabel.text = err;
                    return false;
                }
            }

            return true;
        }
    }
}