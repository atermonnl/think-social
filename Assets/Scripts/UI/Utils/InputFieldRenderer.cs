﻿using Pulssoft.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Arquiz.UI.Utils
{
    [RequireComponent(typeof(InputField))]
    public class InputFieldRenderer : MonoBehaviour
    {
        
        [SerializeField, Required]
        private InputField inputField;
        [SerializeField]
        private Color filledColor;
        [SerializeField]
        private Color emptyColor;
        [SerializeField]
        private Color errorColor;

        private void Awake()
        {
            inputField.onValueChanged.AddListener(SwitchFilledColor);
        }

        public void SetError()
        {
            inputField.targetGraphic.color = errorColor;
        }

        private void SwitchFilledColor(string input)
        {
            inputField.targetGraphic.color = string.IsNullOrEmpty(input) ? emptyColor : filledColor;
        }
    }
}