﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddInteractiveForPopUps : MonoBehaviour
{
    [SerializeField] private Canvas canvasForPopUp;

    public Canvas CanvasForPopUp => canvasForPopUp;

    private void OnEnable()
    {
        Camera aRCamera = FindObjectOfType<Camera>();
        canvasForPopUp.worldCamera = aRCamera;
    }
}
