﻿using Pulssoft.Arquiz.UI.Game;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARSceneManager : MonoBehaviour
{
    [SerializeField]
    private ARSession aRSession;
    [SerializeField]
    private CongratulationScreen congratulationScreen;
    [SerializeField]
    private ResultScreen resultScreen;
    [SerializeField]
    private StatisticScreen statisticScreen;

    [SerializeField]
    private ResultCalculator resultCalculator;

    private float startTime;
    private float timeInGame;

    private int imageScannedCount;
    private int correctAnswersCount;
    private float timeSpent;
    private int countForEndGame;

    public int ImageScannedCount => imageScannedCount;
    public int CorrectAnswersCount => correctAnswersCount;
    public float TimeSpent => timeSpent;

    private void OnEnable()
    {
        startTime = 1 * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        timeInGame += 0.02f;

        if (countForEndGame == 15)
        {
            congratulationScreen.ActivateScreen();
            timeSpent = (timeInGame - startTime) / 60;
        }
    }

    public void GetScannedImageCount()
    {
        imageScannedCount++;
    }

    public void GetCorrectAnswersCount()
    {
        correctAnswersCount++;
    }

    public void GetImageCountForEndGame()
    {
        countForEndGame++;
    }

    public void BackClick()
    {
        StartCoroutine(resultCalculator.SaveResults(null, false, imageScannedCount, resultCalculator.GetTotalScore(correctAnswersCount), 
            resultCalculator.GetBadges(resultCalculator.GetTotalScore(correctAnswersCount))));
    }

    public void ShowStatistic()
    {
        statisticScreen.ImageScanned.text = imageScannedCount.ToString();
        statisticScreen.CorrectAnswers.text = correctAnswersCount.ToString();
        statisticScreen.TimeSpent.text = (timeInGame / 60).ToString();
        statisticScreen.ActivateScreen();
    }

    public void CloseStatistic()
    {
        statisticScreen.DeactivateScreen();
    }
}


