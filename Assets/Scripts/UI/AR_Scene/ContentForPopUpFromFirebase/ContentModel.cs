﻿using System;
using System.Collections.Generic;

[Serializable]
public class ContentModel
{
    public string keyImage;
    public List<Content> content;
    public int correctAnswerIndex;
}
