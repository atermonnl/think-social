﻿public class ARTrackedImageSingleton
{
    public string arTrackedImageName;

    private static ARTrackedImageSingleton instance;

    public static ARTrackedImageSingleton Instance
    {
        get
        {
            if (instance == null)
            {
                if (instance == null)
                    instance = new ARTrackedImageSingleton();
            }
            return instance;
        }
    }

    public static void Reset()
    {
        instance = null;
    }
}
