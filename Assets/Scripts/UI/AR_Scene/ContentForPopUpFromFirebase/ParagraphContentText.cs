﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ParagraphContentText : MonoBehaviour
{
    [SerializeField] private string paragraphKey;
    [SerializeField] private Text paragraph;

    void Start()
    {
        paragraphKey = ARTrackedImageSingleton.Instance.arTrackedImageName;

        if (ARTrackedImageSingleton.Instance.arTrackedImageName != null && ContentSingleton.instance.contentModel != null)
        {
            var list = ContentSingleton.instance.contentModel.Where((content) => content.keyImage == paragraphKey).FirstOrDefault();
            var content = list.content.Where((l) => l.languageName == PlayerPrefs.GetString("language")).FirstOrDefault();

            paragraph.text = content.paragraph;
        }
    }
}
