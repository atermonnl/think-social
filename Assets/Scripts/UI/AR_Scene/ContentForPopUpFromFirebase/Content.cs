using System;

[Serializable]
public class Content
{
    public string languageName;
    public string paragraph;
    public string question;
    public string[] answers;
    public string feedback;
}
