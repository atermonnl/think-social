﻿using System.Collections.Generic;

public class ContentSingleton : BaseSingleton<ContentSingleton>
{
    public List<ContentModel> contentModel { get; set; }
}
