﻿using Firebase.Extensions;
using Firebase.Storage;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ContentManager : MonoBehaviour
{
    private byte[] fileContents;

    public void ContentJsonLoadAndDeserialize(string path)
    {
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;
        StorageReference gsReference = storage.GetReferenceFromUrl(path);

        const long maxAllowedSize = 1 * 1024 * 1024;
        gsReference.GetBytesAsync(maxAllowedSize).ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.LogException(task.Exception);
            }
            else
            {
                fileContents = task.Result;
                string jsonContent = Encoding.Default.GetString(fileContents);
                ContentSingleton.GetInstance().contentModel = JsonConvert.DeserializeObject<List<ContentModel>>(jsonContent);
                Debug.Log("Text or PopUp finished downloading!");
            }
        });
    }
}
