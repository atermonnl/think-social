﻿using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackContentText : MonoBehaviour
{
    [SerializeField] private string feedbackKey;
    [SerializeField] private Text feedback;

    void Start()
    {
        feedbackKey = ARTrackedImageSingleton.Instance.arTrackedImageName;

        if (ARTrackedImageSingleton.Instance.arTrackedImageName != null && ContentSingleton.instance.contentModel != null)
        {
            var list = ContentSingleton.instance.contentModel.Where((content) => content.keyImage == feedbackKey).FirstOrDefault();
            var content = list.content.Where((l) => l.languageName == PlayerPrefs.GetString("language")).FirstOrDefault();

            feedback.text = content.feedback;
        }
    }
}
