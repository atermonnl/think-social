using Unity.Jobs;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Collections.Generic;
using System.Linq;

public class XRReferenceImageFinder : MonoBehaviour
{
    private struct TrackedImage
    {
        public string name;
        public int count;

        public bool toAnswer;
    }

    private ARTrackedImageManager aRTrackedImageManager;

    [SerializeField]
    private ARSession aRSession;
    [SerializeField]
    private XRReferenceImageLibrary referenceImageLibrary;
    [SerializeField]
    private Button libraryBuildButton;

    [SerializeField] private Canvas canvas;
    [SerializeField]
    private GameObject popUpWithTask;

    private string imageName;

    private TrackedImage trackedImage;

    private List<TrackedImage> listOfTrackedImages;

    private List<string> listOfAnswered;

    private bool finishedTask;

    public float widthInMeters;

    //  [SerializeField] private GameObject[] arObjectsToPlace;
    [SerializeField] private GameObject arAnweredToPlace;
     private Dictionary<string, GameObject> arObjects = new Dictionary<string, GameObject>();

    [SerializeField]
    private Vector3 scaleFactor = new Vector3(0.1f,0.1f,0.1f);

    private void Awake()
    {
        aRSession.Reset();
        aRTrackedImageManager = gameObject.AddComponent<ARTrackedImageManager>();
        aRTrackedImageManager.referenceLibrary = aRTrackedImageManager.CreateRuntimeLibrary(referenceImageLibrary);
        aRTrackedImageManager.requestedMaxNumberOfMovingImages = 1;
        listOfTrackedImages = new List<TrackedImage>();
        listOfAnswered = new List<string>();

        // foreach(GameObject arObject in arObjectsToPlace)
        // {
        //     GameObject newArObject = Instantiate(arObject, Vector3.zero, Quaternion.identity);
        //     newArObject.name = arObject.name;
        //     arObjects.Add(arObject.name, newArObject);
        // }
    }

    private void Start()
    {
        aRTrackedImageManager.enabled = true;
    }

    public void AddImageToLibrary()
    {
        if (aRTrackedImageManager.referenceLibrary is MutableRuntimeReferenceImageLibrary mutableLibrary)
        {
            for (int i = 0; i < ImagesSingleton.instance.textures2D.Count; i++)
            {
                if (IndexContentSingleton.instance.contentIndex == 2)
                { imageName = "image" + (i + 21); }
                if (IndexContentSingleton.instance.contentIndex == 1)
                { imageName = "image" + (i + 1); }
                libraryBuildButton.interactable = false;
                mutableLibrary.ScheduleAddImageWithValidationJob(ImagesSingleton.instance.textures2D[i], imageName, widthInMeters);
                aRTrackedImageManager.referenceLibrary = mutableLibrary;

                //Creating 15 ARObjects for ansered tasks for each image
                GameObject obj = Instantiate(arAnweredToPlace, Vector3.zero, Quaternion.identity);
                obj.name = imageName;
                obj.SetActive(false);
                arObjects.Add(imageName, obj);


            }
        }
        else if (aRTrackedImageManager.referenceLibrary == null)
        {
            Debug.LogError("Image library is null.");
        }
        else
        {
            Debug.LogError($"Mutable libraries are not supported. Type of image library is {aRTrackedImageManager.referenceLibrary.GetType().Name})");
        }
    }


    private void OnEnable()
    {
        aRTrackedImageManager.trackedImagesChanged += OnImageTracked;
    }

    private void OnDisable()
    {
        aRTrackedImageManager.trackedImagesChanged -= OnImageTracked;
    }

    private void OnImageTracked(ARTrackedImagesChangedEventArgs imagesChangedEventArgs)
    {

        foreach (ARTrackedImage aRTrackedImage in imagesChangedEventArgs.added)
        {
          UpdateImage(aRTrackedImage);
            trackedImage.name = aRTrackedImage.referenceImage.name;
            trackedImage.toAnswer = true;
            trackedImage.count++;
            finishedTask = false;
           
            if (trackedImage.count < 2)
            {
                ARTrackedImageSingleton.Instance.arTrackedImageName = aRTrackedImage.referenceImage.name;
                GameObject gameobj = Instantiate(popUpWithTask, canvas.transform);
            }else {
                //Listing all tracked images that is tracked on background until a task is completed
                 listOfTrackedImages.Add(trackedImage);
            }
           
            
        }



        foreach (ARTrackedImage aRTrackedImage in imagesChangedEventArgs.updated)
        {
            
               if (aRTrackedImage.trackingState == TrackingState.Tracking )
               {
                   UpdateImage(aRTrackedImage);
                   foreach(TrackedImage image in listOfTrackedImages)
                    {
                        //Showing the task from images that they already tracked once
                        if(aRTrackedImage.referenceImage.name == image.name && image.toAnswer == true && image.count > 1 && finishedTask == true)
                        {
                          finishedTask = false; 
                          trackedImage.count++;
                           ARTrackedImageSingleton.Instance.arTrackedImageName = aRTrackedImage.referenceImage.name;
                           GameObject gameobj = Instantiate(popUpWithTask, canvas.transform);
                        }
                    }
               }

               if (aRTrackedImage.trackingState == TrackingState.Limited)
               {
                 arObjects[aRTrackedImage.referenceImage.name].SetActive(false);
               }

        }   

    }




    public async void EndTrack()
    {
        trackedImage.name = null;
        trackedImage.count = 0;
        trackedImage.toAnswer = false;
        finishedTask = true;

        int index = listOfTrackedImages.FindIndex(c => c.name == ARTrackedImageSingleton.Instance.arTrackedImageName);
        if(index != -1)
        {
            listOfTrackedImages[index] = trackedImage;
        }
        
       
       listOfAnswered.Add(ARTrackedImageSingleton.Instance.arTrackedImageName);

    }

    private void UpdateImage(ARTrackedImage arTrackedImage)
    {
        if(arAnweredToPlace != null)
        {
            
            foreach (string name in listOfAnswered)
            {
                if(arTrackedImage.referenceImage.name == name)
                {
                    // string prefabName = "answered";
                    Vector3 position = arTrackedImage.transform.position;

                    GameObject goARObject = arObjects[arTrackedImage.referenceImage.name];
                    goARObject.SetActive(true);
                    goARObject.transform.position = position;
                    goARObject.transform.localScale = scaleFactor;
                }
            }
        }
        
    }
} 

