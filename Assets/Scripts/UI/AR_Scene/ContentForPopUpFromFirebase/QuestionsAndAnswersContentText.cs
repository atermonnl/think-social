﻿using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestionsAndAnswersContentText : MonoBehaviour
{
    [SerializeField] private string questionKey;
    [SerializeField] private Text question;
    [SerializeField] private Toggle[] toggles; 
    [SerializeField] private Text[] answers;

    private ContentModel model;
    private Content content;

    public ContentModel Model => model;
    public Content Content => content;
    public Toggle[] Toggles => toggles;

    void Start()
    {
        questionKey = ARTrackedImageSingleton.Instance.arTrackedImageName;

        if (ARTrackedImageSingleton.Instance.arTrackedImageName != null && ContentSingleton.instance.contentModel != null)
        {
            model = ContentSingleton.instance.contentModel.Where((content) => content.keyImage == questionKey).FirstOrDefault();
            var content = model.content.Where((l) => l.languageName == PlayerPrefs.GetString("language")).FirstOrDefault();

            question.text = content.question;
            for (int i = 0; i < content.answers.Length; i++)
            {
                answers[i].text = content.answers[i];
                if (content.answers.Length < toggles.Length)
                {
                    toggles[toggles.Length - 1].gameObject.SetActive(false);
                } 
            }
        }
    }
}
