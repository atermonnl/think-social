﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class ARBadgeSceneManager : MonoBehaviour
{
    [SerializeField] private ARSession aRSession;

    private void Awake()
    {
        aRSession.Reset();
    }

    public void BackButtonClick()
    {
        BadgeResultSingleton.Reset();
        SceneManager.LoadScene("Game");
    }
}
