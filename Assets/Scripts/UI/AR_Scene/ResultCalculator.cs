﻿using Pulssoft.Screens;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResultCalculator : MonoBehaviour
{
    [SerializeField]
    private SaveGameResultRequest saveGameResultRequest;

    public IEnumerator SaveResults(AuthUIScreen authUIScreen, bool isDeactiveScreen, int imageScannedCount, int totalScore, string badge)
    {
        saveGameResultRequest.SaveGameResultsRequestMethod(imageScannedCount, 15, totalScore, badge);
        yield return new WaitForSeconds(1);
        if(isDeactiveScreen == true)
        {
            authUIScreen.DeactivateScreen();
        }
        SceneManager.LoadScene("Game");
    }

    public int GetTotalScore(int correctAnswers)
    {
        int totalScore = correctAnswers * 5;
        return totalScore;
    }

    public string GetBadges(int totalScore)
    {
        if (totalScore >= 15 && totalScore < 26)
        {
            return "bronze";
        }
        else if (totalScore >= 26 && totalScore < 51)
        {
            return "silver";
        }
        else if (totalScore >= 51)
        {
            return "gold";
        }
        else
        {
            return "none";
        }
    }
}
