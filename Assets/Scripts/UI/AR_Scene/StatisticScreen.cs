﻿using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

public class StatisticScreen : AuthUIScreen
{
    [SerializeField]
    private Text imageScanned;
    [SerializeField]
    private Text correctAnswers;
    [SerializeField]
    private Text timeSpent;

    public Text ImageScanned { get { return imageScanned; } set { imageScanned = value; } }
    public Text CorrectAnswers { get { return correctAnswers; } set { correctAnswers = value; } }
    public Text TimeSpent { get { return timeSpent; } set { timeSpent = value; } }
}
