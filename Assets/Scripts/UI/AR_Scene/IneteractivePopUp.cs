﻿using Pulssoft.Screens;
using UnityEngine;

public class IneteractivePopUp : AuthUIScreen
{
    private ARSceneManager aRSceneManager;
    private XRReferenceImageFinder xRReferenceImageFinder;

    [SerializeField]
    private TaskPopUp taskPopUp;
    [SerializeField]
    private AnswerPopUp answerPopUp;
    [SerializeField]
    private FeedbackPopUp feedbackPopUp;

    public ARSceneManager ARSceneManager => aRSceneManager;
    public XRReferenceImageFinder XRReferenceImageFinder => xRReferenceImageFinder;

    protected override void Awake()
    {
        base.Awake();

        aRSceneManager = FindObjectOfType<ARSceneManager>();
        xRReferenceImageFinder = FindObjectOfType<XRReferenceImageFinder>();

        taskPopUp.OnClick += answerPopUp.ActivateScreen;
        answerPopUp.OnClick += feedbackPopUp.ActivateScreen;
        feedbackPopUp.OnClick += feedbackPopUp.DeactivateScreen;
    }

    public void DestroyPopup()
    {
        Destroy(this.gameObject);
    }
}