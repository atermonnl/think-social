﻿using System;
using Pulssoft.Screens;
using UnityEngine;

public class FeedbackPopUp : AuthUIScreen
{
    public event Action OnClick;

    [SerializeField] private IneteractivePopUp ineteractivePopUp;

    protected override void Awake()
    {
        base.Awake();

        OnClick += DeactivateScreen;
    }

    public void OnClicked()
    {
        OnClick?.Invoke();
        ineteractivePopUp.ARSceneManager.GetImageCountForEndGame();
        ineteractivePopUp.XRReferenceImageFinder.EndTrack();
        ineteractivePopUp.DestroyPopup();
    }
}
