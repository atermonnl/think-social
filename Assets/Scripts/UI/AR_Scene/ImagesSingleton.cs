﻿using System.Collections.Generic;
using UnityEngine;

public class ImagesSingleton : BaseSingleton<ImagesSingleton>
{
    public List<Texture2D> textures2D { get; set; }
}
