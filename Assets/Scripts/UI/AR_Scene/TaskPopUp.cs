﻿using System;
using Pulssoft.Screens;
using TMPro;
using UnityEngine;

public class TaskPopUp : AuthUIScreen
{
    [SerializeField] private IneteractivePopUp ineteractivePopUp;

    public event Action OnClick;

    protected override void Awake()
    {
        base.Awake();

        OnClick += DeactivateScreen;
    }

    public void OnClicked()
    {
        OnClick?.Invoke();
        ineteractivePopUp.ARSceneManager.GetScannedImageCount();
    }
}
