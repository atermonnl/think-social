﻿using System;
using System.Collections;
using Pulssoft.Screens;
using UnityEngine;
using UnityEngine.UI;

public class AnswerPopUp : AuthUIScreen
{
    public event Action OnClick;

    [SerializeField] private QuestionsAndAnswersContentText questionsAndAnswersContentText;

    [SerializeField] private Image[] background;
    [SerializeField] private Image[] checkmark;

    [SerializeField] private Sprite[] Backgrounds;
    [SerializeField] private Sprite[] Checkmarks;

    [SerializeField] private Button okButton;

    [SerializeField] private IneteractivePopUp ineteractivePopUp;

    protected override void Awake()
    {
        base.Awake();
    }

    public void OnClicked()
    {
        okButton.interactable = false;

        for (int i = 0; i < questionsAndAnswersContentText.Toggles.Length; i++)
        {
            if (questionsAndAnswersContentText.Toggles[i].isOn && i == questionsAndAnswersContentText.Model.correctAnswerIndex)
            {
                ineteractivePopUp.ARSceneManager.GetCorrectAnswersCount();
            }
        }
        StartCoroutine(AnswerTheQuestion());
    }

    private IEnumerator AnswerTheQuestion()
    {
        for (int i = 0; i < questionsAndAnswersContentText.Toggles.Length; i++)
        {
            if (questionsAndAnswersContentText.Toggles[i].isOn && i == questionsAndAnswersContentText.Model.correctAnswerIndex)
            {
                Debug.Log(i + "=" + questionsAndAnswersContentText.Model.correctAnswerIndex);
                ToggleBackgroundChanger(background[questionsAndAnswersContentText.Model.correctAnswerIndex], Backgrounds[0]);
                ToggleCheckmarkChanger(checkmark[questionsAndAnswersContentText.Model.correctAnswerIndex], Checkmarks[0]);
            }
            else
            {
                ToggleBackgroundChanger(background[i], Backgrounds[1]);
                ToggleBackgroundChanger(background[questionsAndAnswersContentText.Model.correctAnswerIndex], Backgrounds[0]);
                ToggleCheckmarkChanger(checkmark[i], Checkmarks[1]);
            }
        }

        yield return new WaitForSeconds(3);
        this.DeactivateScreen();
        OnClick();
    }

    private void ToggleBackgroundChanger(Image background, Sprite currentBackground)
    {
        background.sprite = currentBackground;
    }

    private void ToggleCheckmarkChanger(Image checkmark, Sprite currentCheckmark)
    {
        checkmark.sprite = currentCheckmark;
    }
}



