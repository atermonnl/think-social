﻿using Firebase.Extensions;
using Firebase.Storage;
using Pulssoft.Arquiz.Configs;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ReferenceImageManager : MonoBehaviour
{
    [SerializeField]
    private GameObject loadingScreen;
    [SerializeField]
    private Image logoLoading;

    // public string[] pathsThink_social = new string[] { AppConfig.ReferenceImageContent_Think_Social_img1, AppConfig.ReferenceImageContent_Think_Social_img2, AppConfig.ReferenceImageContent_Think_Social_img3, AppConfig.ReferenceImageContent_Think_Social_img4,
    // AppConfig.ReferenceImageContent_Think_Social_img5, AppConfig.ReferenceImageContent_Think_Social_img6, AppConfig.ReferenceImageContent_Think_Social_img7, AppConfig.ReferenceImageContent_Think_Social_img8, AppConfig.ReferenceImageContent_Think_Social_img9,
    // AppConfig.ReferenceImageContent_Think_Social_img10,AppConfig.ReferenceImageContent_Think_Social_img11, AppConfig.ReferenceImageContent_Think_Social_img12, AppConfig.ReferenceImageContent_Think_Social_img13, AppConfig.ReferenceImageContent_Think_Social_img14,
    // AppConfig.ReferenceImageContent_Think_Social_img15, AppConfig.ReferenceImageContent_Think_Social_img16, AppConfig.ReferenceImageContent_Think_Social_img17, AppConfig.ReferenceImageContent_Think_Social_img18, AppConfig.ReferenceImageContent_Think_Social_img19,
    // AppConfig.ReferenceImageContent_Think_Social_img20 };

       public string[] paths_images = new string[] { AppConfig.ReferenceImageContent_Think_Social_img1, AppConfig.ReferenceImageContent_Think_Social_img2, AppConfig.ReferenceImageContent_Think_Social_img3, AppConfig.ReferenceImageContent_Think_Social_img4, AppConfig.ReferenceImageContent_Think_Social_img5,
    AppConfig.ReferenceImageContent_Think_Social_img6, AppConfig.ReferenceImageContent_Think_Social_img7, AppConfig.ReferenceImageContent_Think_Social_img8, AppConfig.ReferenceImageContent_Think_Social_img9, AppConfig.ReferenceImageContent_Think_Social_img10,
    AppConfig.ReferenceImageContent_Think_Social_img11, AppConfig.ReferenceImageContent_Think_Social_img12, AppConfig.ReferenceImageContent_Think_Social_img13, AppConfig.ReferenceImageContent_Think_Social_img14, AppConfig.ReferenceImageContent_Think_Social_img15,
    AppConfig.ReferenceImageContent_Think_Social_img16, AppConfig.ReferenceImageContent_Think_Social_img17, AppConfig.ReferenceImageContent_Think_Social_img18, AppConfig.ReferenceImageContent_Think_Social_img19, AppConfig.ReferenceImageContent_Think_Social_img20};


    public List<Texture2D> Texture2Ds { get; private set; }

    private byte[] fileContents;

    private void Awake()
    {
        Texture2Ds = new List<Texture2D>();
    }

    public async void ImageJsonLoadAndDeserialize(string[] paths)
    {
        foreach(var path in paths){
                 Debug.Log(path);
        }
       
        
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;
        loadingScreen.SetActive(true);
        for (int i = 0; i < paths.Length; i++)
        {   
            StorageReference gsReference = storage.GetReferenceFromUrl(paths[i]);
               
            const long maxAllowedSize = 1 * 1024 * 1024;
            await gsReference.GetBytesAsync(maxAllowedSize).ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted || task.IsCanceled)
                {
                    Debug.LogException(task.Exception);
                }
                else
                {
                    fileContents = task.Result;
                    Texture2D texture = new Texture2D(200, 200);
                    texture.LoadImage(fileContents);
                    
                    Texture2Ds.Add(texture);
                    logoLoading.fillAmount += 0.05f;
                    Debug.Log("Image" + Texture2Ds.Count);
                }
            });
        }
        SceneManager.LoadScene("Game");
    }
}
