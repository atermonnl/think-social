﻿#if UNITY_IOS
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace PulsSoft.Arquiz.Editor
{
    public static class IOSPostProcessor
    {
        [PostProcessBuild(1)]
        public static void OnPostProcessInfoPlist(BuildTarget buildTarget, string path)
        {
            if (buildTarget != BuildTarget.iOS)
            {
                return;
            }

            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();

            plist.ReadFromString(File.ReadAllText(plistPath));

            PlistElementDict rootDict = plist.root;

            string statusBarStyleKey = "UIStatusBarStyle";
            string viewControllerBasedStatusBarAppearanceKey = "UIViewControllerBasedStatusBarAppearance";
            string requiredComplianceKey = "ITSAppUsesNonExemptEncryption";

            rootDict.SetString(statusBarStyleKey, "UIStatusBarStyleDarkContent");
            rootDict.SetBoolean(viewControllerBasedStatusBarAppearanceKey, false);
            rootDict.SetBoolean(requiredComplianceKey, false);

            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}
#endif