﻿using Pulssoft.Arquiz.UI.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pulssoft.Arquiz.SceneManagers
{
    public class GameSceneManager : MonoBehaviour
    {
        [SerializeField]
        private GameUIManager gameUIManager = null;

        private void Awake()
        {
            gameUIManager.ProfileScreen.LogoutClicked += Logout;
        }

        private void Logout()
        {
            SceneManager.LoadScene("Auth");
        }
    }
}