﻿using Pulssoft.Arquiz.UI.Auth;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pulssoft.Arquiz.SceneManagers
{
    public class AuthSceneManager : MonoBehaviour
    {
        [SerializeField]
        public AuthUIManager authUIManager = null;

        [SerializeField] private NewPasswordScreen newPasswordScreen; 

        private void Awake()
        {
            authUIManager.PasswordScreen.NextClicked += Login;
            if (DeepLinkManager.Instance && DeepLinkManager.Instance.activeFromDeeplink == true)
            {
                newPasswordScreen.ActivateScreen();
            }
        }

        private void Login()
        {
            SceneManager.LoadScene("Game");
        }

        private void SignUp()
        {
            SceneManager.LoadScene("Game");
        }
    }
}