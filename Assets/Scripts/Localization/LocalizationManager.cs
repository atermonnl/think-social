﻿using System;
using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using UnityEngine;

public class LocalizationManager : MonoBehaviour
{
    private void Awake()
    {
        if (PlayerPrefs.GetString("language") == "ENG")
        {
            LoadEnglishLanguage();
        }
        else if (PlayerPrefs.GetString("language") == "CRO")
        {
            LoadCroatianLanguage();
        }
        else if (PlayerPrefs.GetString("language") == "GRE")
        {
            LoadGreekLanguage();
        }
        else if (PlayerPrefs.GetString("language") == "ITA")
        {
            LoadItalianLanguage();
        }
        else if (PlayerPrefs.GetString("language") == "SPA")
        {
            LoadSpanishLanguage();
        }
        else
        {
            LoadEnglishLanguage();
        }

    }

    private void LenguageJsonLoadAndDeserialize(string path, string currentLanguage)
    {
        string jsonLanguage = Resources.Load(path).ToString();
        LocalizationSingleton.Instance.lolizationModel = JsonConvert.DeserializeObject<LocalizationModel>(jsonLanguage);
        LocalizationStateSingleton.Instance.stateLolization = true;
        PlayerPrefs.SetString("language", currentLanguage);
    }

    public void LoadEnglishLanguage()
    {
        LenguageJsonLoadAndDeserialize(AppConfig.EnglishPath, "ENG");
    }

    public void LoadCroatianLanguage()
    {
        LenguageJsonLoadAndDeserialize(AppConfig.CroationPath, "CRO");
    }

    public void LoadGreekLanguage()
    {
        LenguageJsonLoadAndDeserialize(AppConfig.GreekPath, "GRE");
    }

    public void LoadItalianLanguage()
    {
        LenguageJsonLoadAndDeserialize(AppConfig.ItalianPath, "ITA");
    }

    public void LoadSpanishLanguage()
    {
        LenguageJsonLoadAndDeserialize(AppConfig.SpanishPath, "SPA");
    }
}

public class LocalizationStateSingleton
{
    public bool stateLolization = false;

    public static LocalizationStateSingleton instance;

    public static LocalizationStateSingleton Instance
    {
        get
        {
            if (instance == null)
            {
                if (instance == null)
                   instance = new LocalizationStateSingleton();
               
            }
            return instance;
        }
    }

    public static void Reset()
    {
        instance = null;
    }
}
