﻿using Pulssoft.Arquiz.UI.Game;
using UnityEngine;

public class BadgeResultLocalization : MonoBehaviour
{
    [SerializeField]
    private GameObject[] badgesTexts;

    [SerializeField]
    private ResultScreen resultScreen;

    private void OnEnable()
    {
        if (resultScreen.Badge == "gold")
        {
            Activator(true, false, false);
        }
        else if (resultScreen.Badge == "silver")
        {
            Activator(false, true, false);
        }
        else if (resultScreen.Badge == "bronze")
        {
            Activator(false, false, true);
        }
        else
        {
            Activator(false, false, false);
        }
    }

    private void Activator(bool gold, bool silver, bool bronze)
    {
        badgesTexts[0].SetActive(gold);
        badgesTexts[1].SetActive(silver);
        badgesTexts[2].SetActive(bronze);
    }
}
