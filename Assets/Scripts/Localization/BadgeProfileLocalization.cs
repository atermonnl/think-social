﻿using UnityEngine;

public class BadgeProfileLocalization : MonoBehaviour
{
    [SerializeField]
    private GameObject[] badgesTexts;

    private void OnEnable()
    {
        if (AccountSingleton.instance.Account.badge == "gold")
        {
            Activator(true, false, false);
        }
        else if (AccountSingleton.instance.Account.badge == "silver")
        {
            Activator(false, true, false);
        }
        else if (AccountSingleton.instance.Account.badge == "bronze")
        {
            Activator(false, false, true);
        }
        else
        {
            Activator(false, false, false);
        }
    }

    private void Activator(bool gold, bool silver, bool bronze)
    {
        badgesTexts[0].SetActive(gold);
        badgesTexts[1].SetActive(silver);
        badgesTexts[2].SetActive(bronze);
    }
}
