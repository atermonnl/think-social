﻿using System;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class TextForLocalization: MonoBehaviour
{
    [SerializeField]
    private string textKey;

    public string TextKey
    {
        get { return textKey; }
        set { value = textKey; }
    }
    private void OnEnable()
    {
        if (LocalizationStateSingleton.Instance.stateLolization == true)
        {
            Type localizationModelType = Type.GetType("LocalizationModel");
            FieldInfo[] myField = localizationModelType.GetFields();

            var field = myField.Where((x) => x.Name == textKey).FirstOrDefault();
            var loc = LocalizationSingleton.Instance.lolizationModel;
            string value = (string)field.GetValue(loc);
            this.gameObject.GetComponent<Text>().text = value;
        }
    }
}
