﻿using System;

public class LocalizationSingleton 
{
    public LocalizationModel lolizationModel;

    public static LocalizationSingleton instance;
    private static object syncRoot = new Object();

    public static LocalizationSingleton Instance
    {
        get 
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new LocalizationSingleton();
                }
            }
            return instance;
        }
    }

    public static void Reset()
    {
        instance = null;
    }
}
