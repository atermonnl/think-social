﻿namespace Pulssoft.Arquiz.Configs
{
    public class AppConfig
    {
        // TODO : Replace temporary values after real urls provided
        public const string PrivacyPolicyUrl = "http://asserted.eu/sites/default/files/Think%20Social%20AR%20Game%20Privacy%20Policy.pdf";
        public const string TermsOfUseUrl = "http://asserted.eu/sites/default/files/Think%20Social%20AR%20Game%20Privacy%20Policy.pdf";
        public const string RateUsUrl = "http://asserted.eu/sites/default/files/Think%20Social%20AR%20Game%20Privacy%20Policy.pdf";

        // public const string SignInUrl = "https://api.staging.arstop.eu/client/auth/sign-in";
        // public const string SignUpUrl = "https://api.staging.arstop.eu/client/auth/sign-up";
        // public const string ChangePasswordByCodeUrl = "https://api.staging.arstop.eu/client/auth/forgot-password/change";
        // public const string ForgotPasswordUrl = "https://api.staging.arstop.eu/client/auth/forgot-password";
        // public const string GetMyProfileResultsUrl = "https://api.staging.arstop.eu/client/game-results/last";
        // public const string SaveGameResultUrl = "https://api.staging.arstop.eu/client/game-results/";
        // public const string ChangeMyProfileUrl = "https://api.staging.arstop.eu/client/profile/my/update";
        // public const string ChangeMyPasswordUrl = "https://api.staging.arstop.eu/client/profile/password/update";
        // public const string DeleteMyAccountUrl = "https://api.staging.arstop.eu/client/profile/my/delete";
        // public const string GetMyProfileUrl = "https://api.staging.arstop.eu/client/profile/my";

         public const string SignInUrl = "https://thinksocial.erasmus.games/client/auth/sign-in";
        public const string SignUpUrl = "https://thinksocial.erasmus.games/client/auth/sign-up";
        public const string ChangePasswordByCodeUrl = "https://thinksocial.erasmus.games/client/auth/forgot-password/change";
        public const string ForgotPasswordUrl = "https://thinksocial.erasmus.games/client/auth/forgot-password";
        public const string GetMyProfileResultsUrl = "https://thinksocial.erasmus.games/client/game-results/last";
        public const string SaveGameResultUrl = "https://thinksocial.erasmus.games/client/game-results/";
        public const string ChangeMyProfileUrl = "https://thinksocial.erasmus.games/client/profile/my/update";
        public const string ChangeMyPasswordUrl = "https://thinksocial.erasmus.games/client/profile/password/update";
        public const string DeleteMyAccountUrl = "https://thinksocial.erasmus.games/client/profile/my/delete";
        public const string GetMyProfileUrl = "https://thinksocial.erasmus.games/client/profile/my";

        public const string EnglishPath = "Localization/ENG_eng";
        public const string CroationPath = "Localization/CRO_cro";
        public const string GreekPath = "Localization/GRE_gre";
        public const string ItalianPath = "Localization/ITA_ita";
        public const string SpanishPath = "Localization/SPA_spa";

        public const string FirebaseProjectUrl = "gs://thinksocial-8c95c.appspot.com/";

        public const string ContentFolder = "Content/";
  
         public const string Images_Think_Social = "Images_Think_Social/";

        public const string Content_think_social = FirebaseProjectUrl + ContentFolder + "Content_think_social.json";

        #region REFERENCE_IMAGE_THINK_SOCIAL

            // private static string GetImgURLFor_Think_social(string imgName) => FirebaseProjectUrl + Images_Think_Social + imgName;

            // public static string ReferenceImageContent_Think_Social_img1 => GetImgURLFor_Think_social("image1.png");
            // public static string ReferenceImageContent_Think_Social_img2 => GetImgURLFor_Think_social("image2.png");
            // public static string ReferenceImageContent_Think_Social_img3 => GetImgURLFor_Think_social("image3.png");
            // public static string ReferenceImageContent_Think_Social_img4 => GetImgURLFor_Think_social("image4.png");
            // public static string ReferenceImageContent_Think_Social_img5 => GetImgURLFor_Think_social("image5.png");
            // public static string ReferenceImageContent_Think_Social_img6 => GetImgURLFor_Think_social("image6.png");
            // public static string ReferenceImageContent_Think_Social_img7 => GetImgURLFor_Think_social("image7.png");
            // public static string ReferenceImageContent_Think_Social_img8 => GetImgURLFor_Think_social("image8.png");
            // public static string ReferenceImageContent_Think_Social_img9 => GetImgURLFor_Think_social("image9.png");
            // public static string ReferenceImageContent_Think_Social_img10 => GetImgURLFor_Think_social("image10.png");
            // public static string ReferenceImageContent_Think_Social_img11 => GetImgURLFor_Think_social("image11.png");
            // public static string ReferenceImageContent_Think_Social_img12 => GetImgURLFor_Think_social("image12.png");
            // public static string ReferenceImageContent_Think_Social_img13 => GetImgURLFor_Think_social("image13.png");
            // public static string ReferenceImageContent_Think_Social_img14 => GetImgURLFor_Think_social("image14.png");
            // public static string ReferenceImageContent_Think_Social_img15 => GetImgURLFor_Think_social("image15.png");

        // public static string ReferenceImageContent_Think_Social_img1 => GetImgURLFor_Think_social("image21.png");
        // public static string ReferenceImageContent_Think_Social_img2 => GetImgURLFor_Think_social("image22.png");
        // public static string ReferenceImageContent_Think_Social_img3 => GetImgURLFor_Think_social("image23.png");
        // public static string ReferenceImageContent_Think_Social_img4 => GetImgURLFor_Think_social("image24.png");
        // public static string ReferenceImageContent_Think_Social_img5 => GetImgURLFor_Think_social("image25.png");
        // public static string ReferenceImageContent_Think_Social_img6 => GetImgURLFor_Think_social("image26.png");
        // public static string ReferenceImageContent_Think_Social_img7 => GetImgURLFor_Think_social("image27.png");
        // public static string ReferenceImageContent_Think_Social_img8 => GetImgURLFor_Think_social("image28.png");
        // public static string ReferenceImageContent_Think_Social_img9 => GetImgURLFor_Think_social("image29.png");
        // public static string ReferenceImageContent_Think_Social_img10 => GetImgURLFor_Think_social("image30.png");
        // public static string ReferenceImageContent_Think_Social_img11 => GetImgURLFor_Think_social("image31.png");
        // public static string ReferenceImageContent_Think_Social_img12 => GetImgURLFor_Think_social("image32.png");
        // public static string ReferenceImageContent_Think_Social_img13 => GetImgURLFor_Think_social("image33.png");
        // public static string ReferenceImageContent_Think_Social_img14 => GetImgURLFor_Think_social("image34.png");
        // public static string ReferenceImageContent_Think_Social_img15 => GetImgURLFor_Think_social("image35.png");
        // public static string ReferenceImageContent_Think_Social_img16 => GetImgURLFor_Think_social("image36.png");
        // public static string ReferenceImageContent_Think_Social_img17 => GetImgURLFor_Think_social("image37.png");
        // public static string ReferenceImageContent_Think_Social_img18 => GetImgURLFor_Think_social("image38.png");
        // public static string ReferenceImageContent_Think_Social_img19 => GetImgURLFor_Think_social("image39.png");
        // public static string ReferenceImageContent_Think_Social_img20 => GetImgURLFor_Think_social("image40.png");
        private static string GetImgURLFor_Think_social(string imgName) =>  FirebaseProjectUrl + Images_Think_Social + imgName;

        public static string ReferenceImageContent_Think_Social_img1 => GetImgURLFor_Think_social("image21.png");
        public static string ReferenceImageContent_Think_Social_img2 => GetImgURLFor_Think_social("image22.png");
        public static string ReferenceImageContent_Think_Social_img3 => GetImgURLFor_Think_social("image23.png");
        public static string ReferenceImageContent_Think_Social_img4 => GetImgURLFor_Think_social("image24.png");
        public static string ReferenceImageContent_Think_Social_img5 => GetImgURLFor_Think_social("image25.png");
        public static string ReferenceImageContent_Think_Social_img6 => GetImgURLFor_Think_social("image26.png");
        public static string ReferenceImageContent_Think_Social_img7 => GetImgURLFor_Think_social("image27.png");
        public static string ReferenceImageContent_Think_Social_img8 => GetImgURLFor_Think_social("image28.png");
        public static string ReferenceImageContent_Think_Social_img9 => GetImgURLFor_Think_social("image29.png");
        public static string ReferenceImageContent_Think_Social_img10 => GetImgURLFor_Think_social("image30.png");
        public static string ReferenceImageContent_Think_Social_img11 => GetImgURLFor_Think_social("image31.png");
        public static string ReferenceImageContent_Think_Social_img12 => GetImgURLFor_Think_social("image32.png");
        public static string ReferenceImageContent_Think_Social_img13 => GetImgURLFor_Think_social("image33.png");
        public static string ReferenceImageContent_Think_Social_img14 => GetImgURLFor_Think_social("image34.png");
        public static string ReferenceImageContent_Think_Social_img15 => GetImgURLFor_Think_social("image35.png");
        public static string ReferenceImageContent_Think_Social_img16 => GetImgURLFor_Think_social("image36.png");
        public static string ReferenceImageContent_Think_Social_img17 => GetImgURLFor_Think_social("image37.png");
        public static string ReferenceImageContent_Think_Social_img18 => GetImgURLFor_Think_social("image38.png");
        public static string ReferenceImageContent_Think_Social_img19 => GetImgURLFor_Think_social("image39.png");
        public static string ReferenceImageContent_Think_Social_img20 => GetImgURLFor_Think_social("image40.png");
        #endregion
      

       
    }
}