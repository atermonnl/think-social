﻿using System;

[Serializable]
public class ProfileResultResponseViewModel
{
    public string id;

    public int imageScanCount;
    public int totalImageScanCount;
    public int totalScore;
    public string badge;
}
