﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class UnityWebRequestFromExtensions
{
    public static WWWForm SetFormFields(this WWWForm wWWForm, List<String> fields, List<String> values)
    {
        for (int i = 0; i <= fields.Count - 1; i++)
        {
            wWWForm.AddField(fields[i], values[i]);
        }

        return wWWForm;
    }
}
