﻿using Pulssoft.Arquiz.UI.Auth;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeepLinkManager : MonoBehaviour
{
    private string codeFromLimk;
    public string CodeFromLimk => codeFromLimk;

    public static DeepLinkManager Instance { get; private set; }
    public string deeplinkURL;

    public bool activeFromDeeplink = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Application.deepLinkActivated += onDeepLinkActivated;
            if (!String.IsNullOrEmpty(Application.absoluteURL))
            {
                onDeepLinkActivated(Application.absoluteURL);
            }
            else deeplinkURL = "[none]";
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void onDeepLinkActivated(string url)
    {
        deeplinkURL = url;
        string forgotPaswword = "code";
        bool isForgotPaswword = url.Contains(forgotPaswword);
        codeFromLimk = url.Substring(30);
        if (isForgotPaswword == true)
        {
            activeFromDeeplink = true;
            SceneManager.LoadScene("Auth");
        }
    }
}
