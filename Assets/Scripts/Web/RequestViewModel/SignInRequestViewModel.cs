﻿using System;

[Serializable]
public class SignInRequestViewModel
{
    public string email;
    public string password;
}
