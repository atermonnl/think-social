﻿
using System;

[Serializable]
    public class SignUpRequestViewModel
    {
        public string username;
        public string email;
        public string password;
        public string avatarId;
    }
