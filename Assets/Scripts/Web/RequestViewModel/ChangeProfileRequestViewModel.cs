﻿using System;

[Serializable]
public class ChangeProfileRequestViewModel
{
    public string username;
    public string email;
}
