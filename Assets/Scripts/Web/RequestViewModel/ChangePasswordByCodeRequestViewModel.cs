﻿using System;

[Serializable]
public class ChangePasswordByCodeRequestViewModel
{
    public string code;
    public string newPassword;
}
