﻿using System;

[Serializable]
public class GameResultsRequestViewModel
{
    public int imageScanCount;
    public int totalImageScanCount;
    public int totalScore;
    public string badge;
}
