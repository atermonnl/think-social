﻿using System;

[Serializable]
public class ChangePasswordRequestViewModel
{
    public string oldPassword;
    public string newPassword;
}
