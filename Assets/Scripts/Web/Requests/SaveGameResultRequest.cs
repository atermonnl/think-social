﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Arquiz.UI.Game;
using System.Text;
using System;
using System.Collections.Generic;

public class SaveGameResultRequest : MonoBehaviour
{
    [SerializeField] private GameResultsRequestViewModel gameResultsRequestViewModel;

    public void SaveGameResultsRequestMethod(int imageScanCount, int totalImageScanCount, int totalScore, string badge)
    {
        gameResultsRequestViewModel.imageScanCount = imageScanCount;
        gameResultsRequestViewModel.totalImageScanCount = totalImageScanCount;
        gameResultsRequestViewModel.totalScore = totalScore;
        gameResultsRequestViewModel.badge = badge;
        StartCoroutine(SaveGameResults());
    }


    public IEnumerator SaveGameResults()
    {
        string jsonModel = JsonConvert.SerializeObject(gameResultsRequestViewModel);
        UnityWebRequest request = UnityWebRequest.Put(AppConfig.SaveGameResultUrl, jsonModel);
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader(
        "Authorization",
        "Bearer " + TokenSingleton.instance.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("Check error response: " + request.downloadHandler.text);
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
        }
        else
        {
            Debug.Log("Results saved! " + request.downloadHandler.text);
        }
    }
}
