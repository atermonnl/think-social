﻿using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Arquiz.UI.Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChangeProfileRequest : MonoBehaviour
{
    [SerializeField] private ChangeProfileRequestViewModel changeProfileRequestViewModel;
    [SerializeField] private EditProfilePopUp editProfilePopUp;
    [SerializeField] private GetMyProfileWithoutARRequest getMyProfileWithoutARRequest;
    [SerializeField] private ProfileScreen profileScreen;

    public void ChangeProfileRequestMethod()
    {
        if (editProfilePopUp.UsernameInputField.text != null && editProfilePopUp.EmailInputField.text != null  && editProfilePopUp.EmailInputField.text.Length <= 6)
        {
            changeProfileRequestViewModel.username = editProfilePopUp.UsernameInputField.text;
            changeProfileRequestViewModel.email = editProfilePopUp.EmailInputField.text;

            StartCoroutine(ChangeProfile());
        }
    }

    public IEnumerator ChangeProfile()
    {
        WWWForm form = new WWWForm();
        form.SetFormFields(
            new List<String>(new string[] { "username", "email" }),
            new List<String>(new string[] {
                    changeProfileRequestViewModel.username,
                    changeProfileRequestViewModel.email
                })
            );

        UnityWebRequest request = UnityWebRequest.Post(AppConfig.ChangeMyProfileUrl, form);
        request.SetRequestHeader(
        "Authorization",
        "Bearer " + TokenSingleton.instance.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
            Debug.Log(request.downloadHandler.text);
        }
        else
        {
            getMyProfileWithoutARRequest.GetMyProfiletRequestMethod();
            profileScreen.UsernameLabel.text = editProfilePopUp.UsernameInputField.text;
            profileScreen.EmailLabel.text = editProfilePopUp.EmailInputField.text;
            editProfilePopUp.DeactivateScreen();
        }
    }
}
