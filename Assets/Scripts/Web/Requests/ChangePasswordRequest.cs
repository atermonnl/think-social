﻿using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Arquiz.UI.Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChangePasswordRequest : MonoBehaviour
{
    [SerializeField] private ChangePasswordRequestViewModel changePasswordRequestViewModel;
    [SerializeField] private PasswordPopUp passwordPopUp;
    [SerializeField] private GetMyProfileWithoutARRequest getMyProfileWithoutARRequest;

    private void Awake()
    {
        passwordPopUp.CurrentPassword.onValueChanged.AddListener(delegate { CurrentPasswordColorChanger(); });
        passwordPopUp.RepeatNewPassword.onValueChanged.AddListener(delegate { RepeatNewPasswordColorChanger(); });
    }

    public void ChangePasswordRequestMethod()
    {
        changePasswordRequestViewModel.oldPassword = passwordPopUp.CurrentPassword.text;
        changePasswordRequestViewModel.newPassword = passwordPopUp.NewPassword.text;

        if (changePasswordRequestViewModel.oldPassword == PasswordSingleton.instance.password
            && changePasswordRequestViewModel.newPassword == passwordPopUp.RepeatNewPassword.text)
        {
            StartCoroutine(ChangePassword());
        }
        else
        {
            if (changePasswordRequestViewModel.oldPassword != PasswordSingleton.instance.password)
            {
                passwordPopUp.CurrentPassword.textComponent.color = Color.red;
            }

            if (changePasswordRequestViewModel.newPassword != passwordPopUp.RepeatNewPassword.text)
            {
                passwordPopUp.RepeatNewPassword.textComponent.color = Color.red;
            }

        }
    }

    public void CurrentPasswordColorChanger()
    {
        passwordPopUp.CurrentPassword.textComponent.color = Color.black;
    }

    public void RepeatNewPasswordColorChanger()
    {
        passwordPopUp.RepeatNewPassword.textComponent.color = Color.black;
    }

    public IEnumerator ChangePassword()
    {
        WWWForm form = new WWWForm();
        form.SetFormFields(
            new List<String>(new string[] { "oldPassword", "newPassword" }),
            new List<String>(new string[] {
                    changePasswordRequestViewModel.oldPassword,
                    changePasswordRequestViewModel.newPassword
                })
            );

        UnityWebRequest request = UnityWebRequest.Post(AppConfig.ChangeMyPasswordUrl, form);
        request.SetRequestHeader(
        "Authorization",
        "Bearer " + TokenSingleton.instance.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
        }
        else
        {
            Debug.Log("Password is changed");
            passwordPopUp.CurrentPassword.text = null;
            passwordPopUp.NewPassword.text = null;
            passwordPopUp.RepeatNewPassword.text = null;
            PasswordSingleton.instance.password = changePasswordRequestViewModel.newPassword;
            getMyProfileWithoutARRequest.GetMyProfiletRequestMethod();
            passwordPopUp.DeactivateScreen();
        }
    }
}
