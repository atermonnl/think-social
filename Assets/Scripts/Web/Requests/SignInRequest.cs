﻿using Newtonsoft.Json;
using Pulssoft.Arquiz;
using Pulssoft.Arquiz.Configs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SignInRequest : MonoBehaviour
{
    [SerializeField] private SignInRequestViewModel signInRequestmodel;
    [SerializeField] private EmailScreen emailScreen;
    [SerializeField] private AuthErrorPopUp authErrorPopUp;
    [SerializeField] private GetMyProfileRequest getMyProfileRequest;
    [SerializeField] private Button signInButton;

    public void SignInRequestMethod()
    {
        signInButton.interactable = false;
        signInRequestmodel.email = emailScreen.account.email;
        signInRequestmodel.password = emailScreen.account.password;

        StartCoroutine(SignIn());
    }

    public IEnumerator SignIn()
    {
        WWWForm form = new WWWForm();
        form.SetFormFields(
            new List<String>(new string[] { "email", "password" }),
            new List<String>(new string[] {
                    signInRequestmodel.email,
                    signInRequestmodel.password
                })
            );

        UnityWebRequest request = UnityWebRequest.Post(AppConfig.SignInUrl, form);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
            signInButton.interactable = true;
            authErrorPopUp.ActivateScreen();
        }
        else
        {
            PasswordSingleton.GetInstance().password = signInRequestmodel.password;
            TokenResponseViewModel tokenResponseViewModel = JsonConvert.DeserializeObject<TokenResponseViewModel>(request.downloadHandler.text);
            TokenSingleton.GetInstance().token = tokenResponseViewModel.token;
            getMyProfileRequest.GetMyProfileWithARContentRequestMethod();
        }
            
    }
}
 