﻿using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Arquiz.UI.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChangePasswordByCodeRequest : MonoBehaviour
{
    [SerializeField] private ChangePasswordByCodeRequestViewModel changePasswordByCodeRequestViewModel;
    [SerializeField] private NewPasswordScreen newPasswordScreen;

    public void ChangePasswordByCodeRequestMethod()
    {
        changePasswordByCodeRequestViewModel.code = DeepLinkManager.Instance.CodeFromLimk;
        changePasswordByCodeRequestViewModel.newPassword = newPasswordScreen.NewPassword.text;
        StartCoroutine(ChangePasswordByCode());
    }

    public IEnumerator ChangePasswordByCode()
    {
        WWWForm form = new WWWForm();
        form.SetFormFields(
            new List<String>(new string[] { "code", "newPassword" }),
            new List<String>(new string[] {
                    changePasswordByCodeRequestViewModel.code,
                    changePasswordByCodeRequestViewModel.newPassword
                })
            );

        UnityWebRequest request = UnityWebRequest.Post(AppConfig.ChangePasswordByCodeUrl, form);

        yield return request.SendWebRequest();

        Debug.Log("Check response: " + request.downloadHandler.text);

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("Check error response: " + request.downloadHandler.text);
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
            newPasswordScreen.AuthErrorPopUp.ActivateScreen();
        }
        else
        {
            Debug.Log("Form upload complete! " + request.downloadHandler.text);
        }
    }
}

