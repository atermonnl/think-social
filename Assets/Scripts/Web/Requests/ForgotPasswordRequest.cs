﻿using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Arquiz.UI.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ForgotPasswordRequest : MonoBehaviour
{
    [SerializeField] private ForgotPasswordRequestViewModel forgotPasswordRequestViewModel;
    [SerializeField] private ForgotPasswordScreen forgotPasswordScreen;

    public void ForgotPasswordMethod()
    {
        forgotPasswordRequestViewModel.email = forgotPasswordScreen.EmailInputField.text;

        StartCoroutine(ForgotPassword());
    }

    public IEnumerator ForgotPassword()
    {
        WWWForm form = new WWWForm();
        form.SetFormFields(
            new List<String>(new string[] { "email" }),
            new List<String>(new string[] {
                    forgotPasswordRequestViewModel.email
                })
            );

        UnityWebRequest request = UnityWebRequest.Get(AppConfig.ForgotPasswordUrl + "?email=" + forgotPasswordRequestViewModel.email);
        
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
            forgotPasswordScreen.AuthErrorPopUp.ActivateScreen();
        }
        else
        {
            forgotPasswordScreen.AuthErrorPopUp.ActivateScreen();
        }
    }
}
