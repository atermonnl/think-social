﻿using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GetMyProfileRequest : MonoBehaviour
{
    [SerializeField]
    private ReferenceImageManager referenceImageManager;
    [SerializeField]
    private ContentManager contentManager;

    public void GetMyProfileWithARContentRequestMethod()
    {
        StartCoroutine(GetMyProfileWithARContent());
    }

    private IEnumerator GetMyProfileWithARContent()
    {
        UnityWebRequest request = UnityWebRequest.Get(AppConfig.GetMyProfileUrl);
        request.SetRequestHeader(
         "Authorization",
         "Bearer " + TokenSingleton.instance.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("Check error response: " + request.downloadHandler.text);
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
        }
        else
        {
            Debug.Log("Form upload complete! " + request.downloadHandler.text);

            ProfeileResponseViewModel profileModel = JsonConvert.DeserializeObject<ProfeileResponseViewModel>(request.downloadHandler.text);

            Account account = new Account(profileModel.id, profileModel.username,
                profileModel.email, profileModel.avatarId, 0, 0, 0, null);
           
            AccountSingleton.instance.Account = account;

            // TimeSpan birthday = TimeSpan.FromMilliseconds(profileModel.birthday);
            // DateTime startdate = new DateTime(birthday.Ticks);

            // int age = DateTime.Now.Year - startdate.Year;

        

  
                referenceImageManager.ImageJsonLoadAndDeserialize(referenceImageManager.paths_images);
                contentManager.ContentJsonLoadAndDeserialize(AppConfig.Content_think_social);
                IndexContentSingleton.GetInstance().contentIndex = 2;
                ImagesSingleton.GetInstance().textures2D = referenceImageManager.Texture2Ds;
            
        }
    }
}
