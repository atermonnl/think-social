﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using Pulssoft.Arquiz.UI.Auth;
using System;
using System.Collections.Generic;
using Pulssoft.Arquiz.UI.Game;
using UnityEngine.SceneManagement;

public class DeleteAccountRequest : MonoBehaviour
{
    [SerializeField] private DeleteAccountRequestViewModel deleteAccountRequestViewModel;
    [SerializeField] private AccountPopUp deleteAccountPopUp;

    public void DeleteAccountRequestMethod()
    {
        deleteAccountRequestViewModel.password = deleteAccountPopUp.PasswordInputField.text;

        if (deleteAccountRequestViewModel.password == PasswordSingleton.instance.password)
        {
            StartCoroutine(DeleteAccount());
            AccountSingleton.Reset();
            TokenSingleton.Reset();
            IndexContentSingleton.Reset();
            ImagesSingleton.Reset();
            PasswordSingleton.Reset();
        }
        else
        {
            deleteAccountPopUp.tipLabel.text = "Invalid password";
            deleteAccountPopUp.tipLabel.color = Color.red;
        }
    }


    public IEnumerator DeleteAccount()
    {
        WWWForm form = new WWWForm();
        form.SetFormFields(
            new List<String>(new string[] { "password" }),
            new List<String>(new string[] {
                    deleteAccountRequestViewModel.password
                })
            );

        UnityWebRequest request = UnityWebRequest.Post(AppConfig.DeleteMyAccountUrl, form);
        request.SetRequestHeader(
        "Authorization",
        "Bearer " + TokenSingleton.instance.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
        }
        else
        {
            SceneManager.LoadScene("Auth");
        }
    }
}
