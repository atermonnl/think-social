﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using UnityEngine.SceneManagement;
using Pulssoft.Arquiz.UI.Auth;

public class SignUpRequest : MonoBehaviour
{
    [SerializeField] private SignUpRequestViewModel signUpRequestmodel;
    [SerializeField] private UserInfoScreen userInfoScreen;
    [SerializeField] private AuthErrorPopUp authErrorPopUp;
    [SerializeField] private GetMyProfileRequest getMyProfileRequest;
    [SerializeField] private Button signUpButton;

    public void SignUpRequestMethod()
    {
        signUpButton.interactable = false;

        signUpRequestmodel.username = userInfoScreen.account.username;
        signUpRequestmodel.email = userInfoScreen.account.email;
        signUpRequestmodel.password = userInfoScreen.account.password;
        signUpRequestmodel.avatarId = userInfoScreen.account.avatarId;

        PasswordSingleton.GetInstance().password = signUpRequestmodel.password;

        StartCoroutine(SignUp());
    }


    public IEnumerator SignUp()
    {
        WWWForm form = new WWWForm();

        form.SetFormFields(
            new List<String>(new string[] { "username", "email", "password", "avatarId" }),
            new List<String>(new string[] {
                signUpRequestmodel.username,
                signUpRequestmodel.email,
                signUpRequestmodel.password,
                signUpRequestmodel.avatarId
                })
            );

        UnityWebRequest request = UnityWebRequest.Post(AppConfig.SignUpUrl, form);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
            signUpButton.interactable = true;
            authErrorPopUp.ActivateScreen();
        }
        else
        {
            TokenResponseViewModel tokenResponseViewModel = JsonConvert.DeserializeObject<TokenResponseViewModel>(request.downloadHandler.text);
            TokenSingleton.GetInstance().token = tokenResponseViewModel.token;
            getMyProfileRequest.GetMyProfileWithARContentRequestMethod();
        }
    }
}
