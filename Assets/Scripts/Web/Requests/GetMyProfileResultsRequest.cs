﻿using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GetMyProfileResultsRequest : MonoBehaviour
{
    private ProfileResultResponseViewModel profileResultResponseViewModel;

    public ProfileResultResponseViewModel ProfileResultResponseViewModel => profileResultResponseViewModel;

    public void GetMyProfileResultRequestMethod()
    {
        StartCoroutine(GetMyProfileResult());
    }

    public IEnumerator GetMyProfileResult()
    {
        UnityWebRequest request = UnityWebRequest.Get(AppConfig.GetMyProfileResultsUrl);
        request.SetRequestHeader(
         "Authorization",
         "Bearer " + TokenSingleton.instance.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("Check error response: " + request.downloadHandler.text);
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
        }
        else
        {
            Debug.Log("Form upload complete! " + request.downloadHandler.text);

            profileResultResponseViewModel = JsonConvert.DeserializeObject<ProfileResultResponseViewModel>(request.downloadHandler.text);

            if (profileResultResponseViewModel != null)
            {
                Account account = new Account(profileResultResponseViewModel.id, AccountSingleton.instance.Account.username, AccountSingleton.instance.Account.email,
                 AccountSingleton.instance.Account.avatarId, profileResultResponseViewModel.imageScanCount, profileResultResponseViewModel.totalImageScanCount,
                    profileResultResponseViewModel.totalScore, profileResultResponseViewModel.badge);

                AccountSingleton.instance.Account = account;
            }
        }
    }
}
