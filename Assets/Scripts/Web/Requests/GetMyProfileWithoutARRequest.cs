﻿using Newtonsoft.Json;
using Pulssoft.Arquiz.Configs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetMyProfileWithoutARRequest : MonoBehaviour
{
    public void GetMyProfiletRequestMethod()
    {
        StartCoroutine(GetMyProfileContent());
    }

    private IEnumerator GetMyProfileContent()
    {
        UnityWebRequest request = UnityWebRequest.Get(AppConfig.GetMyProfileUrl);
        request.SetRequestHeader(
         "Authorization",
         "Bearer " + TokenSingleton.instance.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("Check error response: " + request.downloadHandler.text);
            BaseErrorResponseViewModel errorModel = JsonConvert.DeserializeObject<BaseErrorResponseViewModel>(request.downloadHandler.text);
        }
        else
        {
            Debug.Log("Form upload complete! " + request.downloadHandler.text);

            ProfeileResponseViewModel profileModel = JsonConvert.DeserializeObject<ProfeileResponseViewModel>(request.downloadHandler.text);

            Account account = new Account(profileModel.id, profileModel.username,
                profileModel.email, profileModel.avatarId, 0, 0, 0, null);

            AccountSingleton.instance.Account = account;
        }
    }
}
