﻿[System.Serializable]

public class Account
{
    public string id;
    public string username;
    public string email;
    public string password;
    public string avatarId;
    public int imageScanCount;
    public int totalImageScanCount;
    public int totalScore;
    public string badge;

    public Account() { }

    public Account(string i, string un, string em, string ai, int isc, int tisc, int ts, string bg)
    {
        id = i;
        username = un;
        email = em;
        avatarId = ai;
        imageScanCount = isc;
        totalImageScanCount = tisc;
        totalScore = ts;
        badge = bg;
    }
}
