﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class FacePrefabRotation : MonoBehaviour
{
    [SerializeField]
    private ARFaceManager aRFaceManager;

    void Start()
    {
        aRFaceManager.facePrefab.transform.rotation = new Quaternion(0, 180, 0, 0);
    }
}
