using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Utils
{
    [RequireComponent(typeof(ScrollRect))]
    public class DynamicScrollLoad : MonoBehaviour
    {
        [SerializeField] DynamicPaginationBehaviour dynamicBehaviour;
        [SerializeField] float loadStartAt = 0.2f;
        private ScrollRect scroll;
        private float lastScrollPos;

        private void Awake()
        {
            scroll = GetComponent<ScrollRect>();
            scroll.onValueChanged.AddListener(ScrollValueChangedHandler);
        }

        void ScrollValueChangedHandler(Vector2 newValue)
        {
            if (scroll.verticalNormalizedPosition > lastScrollPos && lastScrollPos != 0)
                return;

            if (scroll.verticalNormalizedPosition <= loadStartAt &&
                !dynamicBehaviour.DynamicPage.IsLoading &&
                !dynamicBehaviour.DynamicPage.IsFullyLoaded)
            {
                lastScrollPos = scroll.verticalNormalizedPosition;
                dynamicBehaviour.DynamicPage.LoadData();
            }
        }
    }
}