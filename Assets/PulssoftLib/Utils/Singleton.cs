﻿using UnityEngine;
using System.Collections;
using System;

namespace Pulssoft.Utils
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance { get; private set; }

        protected virtual void Awake()
        {
            if (Instance != null)
            {
                throw new InvalidOperationException($"Singleton {typeof(T)} duplication!");
            }
            Instance = GetComponent<T>();
        }
    }
}
