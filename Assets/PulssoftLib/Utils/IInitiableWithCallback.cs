﻿using System;

namespace Pulssoft.Utils
{
    public interface IInitiableWithCallback<T> : IInitable<T>
    {
        event Action<T> CallbackEvent;
    }
}

