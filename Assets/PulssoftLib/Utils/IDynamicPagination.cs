using System.Collections;
using UnityEngine;

namespace Pulssoft.Utils
{
    public interface IDynamicPagination
    {
        bool IsFullyLoaded { get; }
        bool IsLoading { get; }
        int CurrentPage { get; }
        void LoadData();
    }
}