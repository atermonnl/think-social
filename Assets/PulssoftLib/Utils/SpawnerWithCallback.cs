﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Pulssoft.Utils
{
    public abstract class SpawnerWithCallback<T> : Spawner<T>
    {
        private void OnValidate()
        {
            if (initedPrefab && initedPrefab.GetComponent<IInitiableWithCallback<T>>() == null)
            {
                initedPrefab = null;
                Debug.LogError($"{nameof(initedPrefab)} needs {nameof(IInitiableWithCallback<T>)} component!");
            }
        }

        protected override void InitNewChild(GameObject child, T data)
        {
            base.InitNewChild(child, data);
            child.GetComponent<IInitiableWithCallback<T>>().CallbackEvent += HandleCallback;
        }

        protected abstract void HandleCallback(T data);
    }
}
