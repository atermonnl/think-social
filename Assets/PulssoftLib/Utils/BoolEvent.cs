﻿using System;
using UnityEngine.Events;

namespace Pulssoft.Utils
{
    [Serializable]
    public class BoolEvent : UnityEvent<bool>
    {
    }
}
