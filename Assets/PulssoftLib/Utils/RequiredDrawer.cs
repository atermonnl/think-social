﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Pulssoft.Utils
{
    [CustomPropertyDrawer(typeof(RequiredAttribute))]
    class RequiredDrawer : ReadOnlyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            base.OnGUI(position, property, label);
            
        }
    }
}
#endif