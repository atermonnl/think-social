﻿using UnityEngine;
using System.Collections;
using Pulssoft.Event.Model;

namespace Pulssoft.Utils
{
    public class Settings : MonoBehaviour
    {
        [SerializeField]
        uint framerate = 60;
        [SerializeField]
        int timeout = -1;

        void Start()
        {
            Screen.sleepTimeout = timeout;
            Application.targetFrameRate = (int)framerate;
        }
    }
}
