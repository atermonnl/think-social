﻿using UnityEngine;

namespace Pulssoft.Utils
{
    public class Spawner<T> : MonoBehaviour, IInitable<T[]>
    {
        [SerializeField]
        protected GameObject initedPrefab;

        [SerializeField]
        Transform content;


        private void OnValidate()
        {
            if (initedPrefab && initedPrefab.GetComponent<IInitable<T>>() == null)
            {
                initedPrefab = null;
                Debug.LogError($"{nameof(initedPrefab)} needs {nameof(IInitable<T>)} component!");
            }
        }

        public void Init(T[] dataArray)
        {
            for (int i = 0; i < dataArray.Length; i++)
            {
                SetData(dataArray[i]);
            }
        }

        public void SetData(T data)
        {
            var newGameObj = Instantiate(initedPrefab, content, false);
            newGameObj.transform.SetAsLastSibling();
            InitNewChild(newGameObj, data);
        }

        protected virtual void InitNewChild(GameObject child, T data)
        {
            var initable = child.GetComponent<IInitable<T>>();
            initable.Init(data);
        }
    }
}
