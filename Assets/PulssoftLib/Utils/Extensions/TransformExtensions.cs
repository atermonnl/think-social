﻿using UnityEngine;
using UnityEditor;

namespace Pulssoft.Utils
{
    public static class TransformExt
    {
        public static void DestroyChildren(this Transform transform)
        {
            for (int i = 0; i < transform.childCount; i++)
                Object.Destroy(transform.GetChild(i).gameObject);
        }
    }
}