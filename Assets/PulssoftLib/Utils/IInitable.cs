﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulssoft.Utils
{
    public interface IInitable<T>
    {
        void Init(T data);
    }

    public interface IInitable<T0, T1>
    {
        void Init(T0 data0, T1 data1);
    }

    public interface IInitable<T0, T1, T2>
    {
        void Init(T0 data0, T1 data1, T2 data2);
    }

    public interface IInitable<T0, T1, T2, T3>
    {
        void Init(T0 data0, T1 data1, T2 data2, T3 data3);
    }

    public interface IArrayInitable<T>
    {
        void Init(T[] data);
    }
}

