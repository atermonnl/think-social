using System.Collections;
using UnityEngine;

namespace Pulssoft.Utils
{
    [RequireComponent(typeof(IDynamicPagination))]
    public class DynamicPaginationBehaviour : MonoBehaviour
    {
        public IDynamicPagination DynamicPage { get; private set; }

        private void Awake()
        {
            DynamicPage = GetComponent<IDynamicPagination>();
        }
    }
}