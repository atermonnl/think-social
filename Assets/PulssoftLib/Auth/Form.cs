﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace Pulssoft.Auth
{
    public abstract class Form : MonoBehaviour
    {
        [SerializeField] Text errorLabel;
        [SerializeField] FieldValidator[] fields;

        public FieldValidator[] Fields { get => fields; }

        public void Send()
        {
            if (ValidateAll())
                SendToBackEnd(SetError);
        }

        private void OnEnable()
        {
            errorLabel.text = string.Empty;
        }

        protected abstract void SendToBackEnd(Action<string> callback);

        bool ValidateAll()
        {
            foreach (var field in Fields)
            {
                var err = field.Validate();
                if (err != null)
                {
                    errorLabel.text = err;
                    return false;
                }
            }

            return true;
        }

        void SetError(string error)
        {
            if (error != null)
                errorLabel.text = error;
        }
    }
}