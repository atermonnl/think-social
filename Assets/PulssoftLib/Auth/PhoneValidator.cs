﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

namespace Pulssoft.Auth
{
    public class PhoneValidator : EmptyFieldValidator
    {
        public override string Validate()
        {
            var baseErr = base.Validate();
            if (baseErr != null)
                return baseErr;

            Match matches = Regex.Match(IField.text, @"^(?<countryCode>[\+][1-9]{1}[0-9]{0,2}\s)?(?<areaCode>0?[1-9]\d{0,4})(?<number>\s[1-9][\d]{5,12})(?<extension>\sx\d{0,4})?$");
            if (matches.Success)
                return null;
            else
                return "Not valid phone!";
        }
    }
}
