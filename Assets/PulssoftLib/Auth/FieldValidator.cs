﻿using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.Auth
{
    [RequireComponent(typeof(InputField))]
    public abstract class FieldValidator : MonoBehaviour
    {
        public InputField IField { get; private set; }

        private void Awake()
        {
            IField = GetComponent<InputField>();
        }

        public abstract string Validate();
    }
}