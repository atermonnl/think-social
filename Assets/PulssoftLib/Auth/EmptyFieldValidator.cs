﻿using UnityEngine;
using System.Collections;

namespace Pulssoft.Auth
{
    public class EmptyFieldValidator : FieldValidator
    {
        public override string Validate()
        {
            if (IField.text == string.Empty)
                return "Field '" + name + "' is empty.";

            return null;
        }
    }
}