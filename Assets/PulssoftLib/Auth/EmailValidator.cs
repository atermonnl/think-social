﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

namespace Pulssoft.Auth
{
    public class EmailValidator : EmptyFieldValidator
    {
        public override string Validate()
        {
            var baseErr = base.Validate();
            if (baseErr != null)
                return baseErr;

            string regexPattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
            + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
            Match matches = Regex.Match(IField.text, regexPattern);
            if (matches.Success)
                return null;
            else
                return "The email address is badly formatted.";
        }
    }
}