﻿#if UNITY_PURCHASING
using Pulssoft.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

namespace Pulssoft.IAPs.Subscription
{
    public class Subscribe : Singleton<Subscribe>
    {
        public const string iapKey = "iap";

        public static bool IsActive { get; private set; }
        public event Action OnStatusChanged;


        IEnumerator Start()
        {
            if (PlayerPrefs.HasKey(iapKey))
            {
                yield return new WaitUntil(() => CodelessIAPStoreListener.initializationComplete);
                bool validPurchase = false; // Presume valid for platforms with no R.V.
#if validate_receipt

                // Unity IAP's validation logic is only included on these platforms.
                //#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
                // Prepare the validator with the secrets we prepared in the Editor
                // obfuscation window.
                var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                    AppleTangle.Data(), Application.identifier);
#endif
                try
                {
#if validate_receipt
                    // On Google Play, result has a single product ID.
                    // On Apple stores, receipts contain multiple products.
                    var result = validator.Validate(PlayerPrefs.GetString(Subscribe.iapKey));
                    // For informational purposes, we list the receipt(s)
                    Debug.Log("Receipt is valid. Contents:");
                    foreach (IPurchaseReceipt productReceipt in result)
                    {
                        Debug.Log(productReceipt.productID);
                        Debug.Log(productReceipt.purchaseDate);
                        Debug.Log(productReceipt.transactionID);
                    }
#endif
                    bool hasOneActive = false;

                    var m_AppleExtensions = CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IAppleExtensions>();
                    var introductory_info_dict = m_AppleExtensions?.GetIntroductoryPriceDictionary();

                    foreach (var product in CodelessIAPStoreListener.Instance.StoreController.products.all)
                    {
                        if (product.receipt != null)
                        {
                            if (product.definition.type == ProductType.Subscription)
                            {
                                if (checkIfProductIsAvailableForSubscriptionManager(product.receipt))
                                {
                                    string intro_json = (introductory_info_dict == null || !introductory_info_dict.ContainsKey(product.definition.storeSpecificId)) ? null : introductory_info_dict[product.definition.storeSpecificId];
                                    var productInfo = new SubscriptionManager(product, intro_json);

                                    if (productInfo.getSubscriptionInfo().isSubscribed() == Result.True)
                                    {
                                        hasOneActive = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    validPurchase = hasOneActive;
                }
                catch (IAPSecurityException)
                {
                    Debug.Log("Invalid receipt, not unlocking content");
                }

                if (validPurchase)
                {
                    Activate();
                }
            }
        }

        private bool checkIfProductIsAvailableForSubscriptionManager(string receipt)
        {
            var receipt_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
            if (!receipt_wrapper.ContainsKey("Store") || !receipt_wrapper.ContainsKey("Payload"))
            {
                Debug.Log("The product receipt does not contain enough information");
                return false;
            }
            var store = (string)receipt_wrapper["Store"];
            var payload = (string)receipt_wrapper["Payload"];

            if (payload != null)
            {
                switch (store)
                {
                    case GooglePlay.Name:
                        {
                            var payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
                            if (!payload_wrapper.ContainsKey("json"))
                            {
                                Debug.Log("The product receipt does not contain enough information, the 'json' field is missing");
                                return false;
                            }
                            var original_json_payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode((string)payload_wrapper["json"]);
                            if (original_json_payload_wrapper == null || !original_json_payload_wrapper.ContainsKey("developerPayload"))
                            {
                                Debug.Log("The product receipt does not contain enough information, the 'developerPayload' field is missing");
                                return false;
                            }
                            var developerPayloadJSON = (string)original_json_payload_wrapper["developerPayload"];
                            var developerPayload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(developerPayloadJSON);
                            if (developerPayload_wrapper == null || !developerPayload_wrapper.ContainsKey("is_free_trial") || !developerPayload_wrapper.ContainsKey("has_introductory_price_trial"))
                            {
                                Debug.Log("The product receipt does not contain enough information, the product is not purchased using 1.19 or later");
                                return false;
                            }
                            return true;
                        }
                    case AppleAppStore.Name:
                    case AmazonApps.Name:
                    case MacAppStore.Name:
                        {
                            return true;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }
            return false;
        }
        
        public void Activate(Product product)
        {
            PlayerPrefs.SetString(iapKey, product.receipt);
            PlayerPrefs.Save();
            Activate();
        }

        public void Activate()
        {
            IsActive = true;
            OnStatusChanged?.Invoke();
        }
    }
}
#endif