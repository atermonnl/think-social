﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Pulssoft.Remote.Extensions
{
    public static class ScreenExt
    {
        public static void SendPostRequestText(this Screens.Screen sender, string url, Dictionary<string, string> keyValueData, ResponseHandler handleResponse)
        {
            sender.StartCoroutine(RequestManager.Instance.SendPostRequestText(url, keyValueData, handleResponse));
        }

        public static void SendPostRequestForm(this Screens.Screen sender, string url, string authToken, Dictionary<string, string> formDataText, Dictionary<string, byte[]> formDataFiles, ResponseHandler handleResponse)
        {
            sender.StartCoroutine(RequestManager.Instance.SendPostRequestForm(url, authToken, formDataText, formDataFiles, handleResponse));
        }

        public static void SendGetRequestForm(this Screens.Screen sender, string url, string authToken, Dictionary<string, string> keyValueData, ResponseHandler handleResponse)
        {
            sender.StartCoroutine(RequestManager.Instance.SendGetRequest(url, authToken, keyValueData, handleResponse));
        }

        public static void SendDeleteRequest(this Screens.Screen sender, string url, string authToken, Dictionary<string, string> keyValueData, ResponseHandler handleResponse)
        {
            sender.StartCoroutine(RequestManager.Instance.SendDeleteRequest(url, authToken, keyValueData, handleResponse));
        }
    }
}