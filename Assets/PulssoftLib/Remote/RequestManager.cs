﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Networking;
using Pulssoft.Utils;

namespace Pulssoft.Remote
{
    public delegate void ResponseHandler(UnityWebRequest response);

    public class RequestManager : Singleton<RequestManager>
    {
        protected string contentType = "application/json";
        protected string apiUrl = "";
        protected int timeout = 15;

        public IEnumerator SendPostRequestText(string url, Dictionary<string, string> keyValueData, ResponseHandler handleResponse)
        {
            string requestText = string.Empty;
            foreach (var keyValuePair in keyValueData)
                requestText += keyValuePair.Value + "&";

            requestText = requestText.TrimEnd('&');

            Debug.Log("POST data: " + requestText);

            using (UnityWebRequest request = UnityWebRequest.Put(url, Encoding.ASCII.GetBytes(requestText.ToCharArray())))
            {
                request.timeout = timeout;
                request.method = UnityWebRequest.kHttpVerbPOST;
                request.SetRequestHeader("Content-Type", contentType);

                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log("Code: " + request.responseCode);
                    Debug.Log("Output: " + request.downloadHandler.text);

                }
                else
                {
                    Debug.Log("Request successful. Output: " + request.downloadHandler.text);
                }

                if (request != null && Application.internetReachability != NetworkReachability.NotReachable)
                    handleResponse(request);
                else
                    Debug.Log("Response is empty");
            }
        }

        public IEnumerator SendPostRequestForm(string url, string authToken, Dictionary<string, string> formDataText, Dictionary<string, byte[]> formDataFiles, ResponseHandler handleResponse)
        {
            WWWForm wwwForm = new WWWForm();

            foreach (var keyValuePair in formDataText)
            {
                wwwForm.AddField(keyValuePair.Key, keyValuePair.Value);
            }
            foreach (var keyValuePair in formDataFiles)
            {
                wwwForm.AddBinaryData(keyValuePair.Key, keyValuePair.Value);
            }

            using (UnityWebRequest request = UnityWebRequest.Post(url, wwwForm))
            {
                request.timeout = timeout;
                if (authToken != null)
                    request.SetRequestHeader("Authorization", "Bearer " + authToken);

                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log("Code: " + request.responseCode);
                    Debug.Log("Output: " + request.downloadHandler.text);
                }
                else
                {
                    Debug.Log("Code: " + request.responseCode);
                    Debug.Log("Request successful. Output: " + request.downloadHandler.text);
                }

                if (request != null && Application.internetReachability != NetworkReachability.NotReachable)
                    handleResponse(request);
                else
                    Debug.Log("Response is empty");
            }
        }

        public IEnumerator SendGetRequest(string url, string authToken, Dictionary<string, string> keyValueData, ResponseHandler handleResponse)
        {
            string form = "?";
            string fullUrl = url;

            if (keyValueData != null)
            {
                foreach (var keyValuePair in keyValueData)
                    form += keyValuePair.Key + "=" + keyValuePair.Value + "&";

                form = form.TrimEnd('&');

                fullUrl += form;
            }

            using (UnityWebRequest request = UnityWebRequest.Get(fullUrl))
            {
                request.timeout = timeout;
                request.SetRequestHeader("Content-Type", contentType);

                if (authToken != null)
                    request.SetRequestHeader("Authorization", "Bearer " + authToken);

                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log("Code: " + request.responseCode);
                    Debug.Log("Output: " + request.downloadHandler.text);
                }
                else
                {
                    Debug.Log("Request successful. Output: " + request.downloadHandler.text);
                }

                if (request != null && Application.internetReachability != NetworkReachability.NotReachable)
                    handleResponse(request);
                else
                    Debug.Log("Response is empty");
            }
        }

        public IEnumerator SendDeleteRequest(string url, string authToken, Dictionary<string, string> keyValueData, ResponseHandler handleResponse)
        {
            string requestText = string.Empty;
            foreach (var keyValuePair in keyValueData)
                requestText += keyValuePair.Value + "&";
            requestText = requestText.TrimEnd('&');

            using (UnityWebRequest request = UnityWebRequest.Put(url, Encoding.ASCII.GetBytes(requestText.ToCharArray())))
            {
                request.timeout = timeout;
                request.method = UnityWebRequest.kHttpVerbDELETE;
                if (authToken != null)
                    request.SetRequestHeader("Authorization", "Bearer " + authToken);

                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log("Code: " + request.responseCode);
                    Debug.Log("Output: " + request.downloadHandler.text);
                }
                else
                {
                    Debug.Log("Request successful. Output: " + request.downloadHandler.text);
                }

                if (request != null && Application.internetReachability != NetworkReachability.NotReachable)
                    handleResponse(request);
                else
                    Debug.Log("Response is empty");
            }
        }
    }
}