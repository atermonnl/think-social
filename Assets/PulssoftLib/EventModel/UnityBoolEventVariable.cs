﻿using Pulssoft.Utils;
using System;

namespace Pulssoft.Event.Model
{
    [Serializable]
    public struct UnityBoolEventVariable
    {
        private bool value;

        public BoolEvent ValueChanged;
        public bool Value
        {
            get => value;
            set
            {
                if (this.value.Equals(value))
                    return;

                this.value = value;
                ValueChanged.Invoke(value);
            }
        }
    }
}
