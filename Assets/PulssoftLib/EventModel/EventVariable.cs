﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Pulssoft.Event.Model
{
    public struct EventVariable<T>
    {
        private T value;

        public ValueDelegate<T> ValueChanged;
        public T Value
        {
            get => value;
            set
            {
                if (this.value == null)
                { 
                    if (value == null)
                        return;
                }
                else if (this.value.Equals(value))
                    return;

                this.value = value;
                ValueChanged?.Invoke(value);
            }
        }
    }
}
