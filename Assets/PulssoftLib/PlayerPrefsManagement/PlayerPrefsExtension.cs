﻿using UnityEngine;

namespace Pulssoft.PlayerPrefsManagement
{
    public static class PlayerPrefExtension
    {
        public static string GetString(this PlayerPref pref)
        {
            return PlayerPrefs.GetString(pref.ToString());
        }

        public static int GetInt(this PlayerPref pref)
        {
            return PlayerPrefs.GetInt(pref.ToString());
        }

        public static float GetFloat(this PlayerPref pref)
        {
            return PlayerPrefs.GetFloat(pref.ToString());
        }

        public static void SetString(this PlayerPref pref, string value)
        {
            PlayerPrefs.SetString(pref.ToString(), value);
        }

        public static void SetInt(this PlayerPref pref, int value)
        {
            PlayerPrefs.SetInt(pref.ToString(), value);
        }

        public static void SetFloat(this PlayerPref pref, float value)
        {
            PlayerPrefs.SetFloat(pref.ToString(), value);
        }
    }
}