﻿using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;
using System;
using System.Collections;

namespace Pulssoft.Input
{ 
    public class TouchInputController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        enum InputState
        {
            None,
            Drag,
            Zoom,
            Pan
        }



        [SerializeField]
        InputState currentState;

        [SerializeField]
        float holdTime = 0.5f;

        [SerializeField]
        float maxTapDuration = 0.25f;

        public event Action<Vector2> OnTaped;
        public event Action<Vector2> OnDraged;
        public event Action<Vector2> OnPaned;
        public event Action<Vector2> OnHold;
        public event Action<float> OnZoomed;

        private Camera cam;
        private Vector3 prevPanPos;
        private float camDistance;
        private float pointerDownTime;
        private Coroutine cor;
        private bool holdCorStarted;


        void Awake()
        {
            cam = Camera.main;
            camDistance = cam.orthographic ? cam.orthographicSize : cam.transform.position.z;
        }

    #if UNITY_EDITOR || UNITY_STANDALONE
        void Update()
        {
            if (UnityEngine.Input.mouseScrollDelta.y != 0)
            {
                currentState = InputState.Zoom;
                OnZoomed?.Invoke(UnityEngine.Input.mouseScrollDelta.y * 10f);
            }
            else if (currentState == InputState.Zoom)
                currentState = InputState.None;
        }
    #endif


        public void OnPointerUp(PointerEventData eventData)
        {
            if (cor != null)
                StopCoroutine(cor);

            if (currentState == InputState.None && Time.time - pointerDownTime <= maxTapDuration)
                OnTaped?.Invoke(eventData.position);
    #if !UNITY_EDITOR && !UNITY_STANDALONE
                else if (UnityEngine.Input.touchCount == 1)
                    currentState = InputState.None;
    #else
            currentState = InputState.None;
    #endif
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            pointerDownTime = Time.time;
            cor = StartCoroutine(HoldCor(eventData.position));
        }

        public void OnDrag(PointerEventData eventData)
        {

    #if UNITY_EDITOR || UNITY_STANDALONE
            if (UnityEngine.Input.GetMouseButton(1))
            {
                var mousePos = UnityEngine.Input.mousePosition;

                Vector3 inputPosition = cam.ScreenToWorldPoint(new Vector3(-mousePos.x, mousePos.y, camDistance));
                if (currentState != InputState.Pan)
                    prevPanPos = inputPosition;
                currentState = InputState.Pan;
                var direction = inputPosition - prevPanPos;
                prevPanPos = inputPosition;
                OnPaned?.Invoke(direction);
            }
            else if (UnityEngine.Input.GetMouseButton(0))
            {
                currentState = InputState.Drag;
                OnDraged?.Invoke(eventData.delta);
            }
#else
                if (UnityEngine.Input.touchCount == 1)
                {
                    currentState = InputState.Drag;
                    OnDraged?.Invoke(eventData.delta);
                }
                else
                {
                    var touch0 = UnityEngine.Input.GetTouch(0);
                    var touch1 = UnityEngine.Input.GetTouch(1);

                    if (Vector2.Angle(touch0.deltaPosition, touch1.deltaPosition) < 90f)
                    {
                        var centredPos = (touch0.position + touch1.position) / 2;
                        Vector3 realPos = cam.ScreenToWorldPoint(new Vector3(centredPos.x, centredPos.y, camDistance));
                        if (currentState != InputState.Pan)
                            prevPanPos = realPos;
                        var direction = realPos - prevPanPos;
                        prevPanPos = realPos;
                        currentState = InputState.Pan;
                        OnPaned?.Invoke(direction);
                    }
                    else
                    {
                        Vector2 touch0PrevPos = touch0.position - touch0.deltaPosition;
                        Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
                        float prevTouchDeltaMag = (touch0PrevPos - touch1PrevPos).magnitude;
                        float touchDeltaMag = (touch0.position - touch1.position).magnitude;
                        currentState = InputState.Zoom;
                        OnZoomed?.Invoke(prevTouchDeltaMag - touchDeltaMag);
                    }
                }
#endif
        }

        IEnumerator HoldCor(Vector2 pos)
        {
            if (holdCorStarted)
                yield break;

            holdCorStarted = true;
            while (Time.time - pointerDownTime < holdTime)
            {
                if (currentState != InputState.None)
                    yield break;

                yield return null;
            }
            OnHold?.Invoke(pos);
            holdCorStarted = false;
        }
    }
}