﻿using System;
using Pulssoft.Arquiz.Configs;
using UnityEngine;

namespace Pulssoft.Screens
{
    public class AuthUIScreen : Screen
    {
        public event Action BackClicked;
        public event Action NextClicked;

        protected virtual void Awake()
        {
            BackClicked += DeactivateScreen;
            NextClicked += DeactivateScreen;
        }

        public void HandleBackClick()
        {
            BackClicked?.Invoke();
        }

        public void HandleNextClick()
        {
            NextClicked?.Invoke();
        }

        public void OpenPrivacyPolicy()
        {
            Application.OpenURL(AppConfig.PrivacyPolicyUrl);
        }

        public void OpenTermsOfUse()
        {
            Application.OpenURL(AppConfig.TermsOfUseUrl);
        }
    }
}

