﻿using Pulssoft.Utils;
using System;
using UnityEngine;

namespace Pulssoft.Screens
{
    public class Screen : MonoBehaviour
    {
        [SerializeField]
        protected int index;

        [SerializeField, Required]
        protected RectTransform rectTransform;

        public int Index { get => index; }
        public RectTransform RectTransform { get => rectTransform; }


        public virtual void ActivateScreen()
        {
            gameObject.SetActive(true);
        }

        public virtual void DeactivateScreen()
        {
            gameObject.SetActive(false);
        }

        public virtual void StopAllJobs()
        {
            StopAllCoroutines();
        }
    }
}
