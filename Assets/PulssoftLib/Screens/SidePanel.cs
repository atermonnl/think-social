﻿#if DOTWEEN
using UnityEngine;
using System.Collections;

using DG.Tweening;


namespace Pulssoft.Screens
{
    public class SidePanel : Screen
    {
        [SerializeField]
        RectTransform mainPanel;

        [SerializeField]
        float switchTime = 0.5f;

        [SerializeField]
        float mainPanelMoveTime = 2f;

        public override void ActivateScreen()
        {
            base.ActivateScreen();
            RectTransform.anchoredPosition = new Vector2(UnityEngine.Screen.width, 0);
            var mainPanelTweener = mainPanel.DOLocalMoveX(-UnityEngine.Screen.width, mainPanelMoveTime);
            RectTransform.DOLocalMoveX(0, switchTime).OnComplete(() =>
            {
                mainPanelTweener.Kill();
                mainPanel.gameObject.SetActive(false);
            });
        }

        public void HidePanel()
        {
            StopAllJobs();
            mainPanel.gameObject.SetActive(true);

            RectTransform.DOLocalMoveX(UnityEngine.Screen.width, switchTime);
            mainPanel.DOLocalMoveX(0, switchTime);
        }
    }
}

#endif
