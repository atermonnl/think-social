﻿using Pulssoft.Arquiz.UI.Game;
using UnityEngine;

namespace Pulssoft.Screens
{
    public class NavigationScreen : Screen
    {
        [SerializeField]
        private NavigationBar navigationBar = null;

        public NavigationBar NavigationBar => navigationBar;
    }
}