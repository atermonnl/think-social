﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pulssoft.UIExtensions
{
    public class ScrollHandler : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerClickHandler
    {
        private const float maxDelta = 5f;

        [SerializeField]
        ScrollRect mainScrollRect;

        [SerializeField]
        ScrollRect scrollRect;

        private bool isInUse;
        private bool isSwiped;

        private void Awake()
        {
            mainScrollRect = GetComponentInParent<ScrollRect>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (Mathf.Abs(eventData.delta.y) > maxDelta)
            {
                isInUse = true;
                mainScrollRect.OnBeginDrag(eventData);
            }
            else
            {
                scrollRect.OnBeginDrag(eventData);
            }

            isSwiped = true;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (isInUse)
            {
                mainScrollRect.OnEndDrag(eventData);
                isInUse = false;
            }
            else
            {
                scrollRect.OnEndDrag(eventData);
            }


            isSwiped = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (isInUse)
            {
                mainScrollRect.OnDrag(eventData);
            }
            else
                scrollRect.OnDrag(eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (isSwiped)
                return;

            var resList = new System.Collections.Generic.List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, resList);
            foreach (var result in resList)
            {
                if (result.gameObject.TryGetComponent(out IPointerClickHandler pointerClick))
                {
                    if (pointerClick != this)
                        pointerClick.OnPointerClick(eventData);
                }
            }
        }
    }
}
