﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

namespace Pulssoft.UIExtensions
{
    public class ToggleExtended : Toggle
    {
        ToggleGroup tGroup;

        public void Init(ToggleGroup tGroup)
        {
            this.tGroup = tGroup;
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (tGroup && !tGroup.allowSwitchOff)
            {
                var active = tGroup.ActiveToggles();
                if (active.Count() == 1 && active.First() == this && isOn)
                    return;
            }
            base.OnPointerClick(eventData);
        }
    }
}