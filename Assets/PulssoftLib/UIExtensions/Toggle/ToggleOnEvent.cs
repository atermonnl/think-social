﻿using Pulssoft.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.UIExtensions
{
    public class ToggleOnEvent : MonoBehaviour
    {
        void Awake()
        {
            toggle.onValueChanged.AddListener(v => { if (v) OnToggleIsOn.Invoke(); });
        }

        [SerializeField] Toggle toggle;
        public ActionEvent OnToggleIsOn;
    }
}