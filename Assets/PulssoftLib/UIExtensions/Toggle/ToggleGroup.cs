﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Pulssoft.UIExtensions
{
    [DisallowMultipleComponent]
    public class ToggleGroup : UIBehaviour
    {
        [Serializable]
        public class ToggleEvent : UnityEvent<Toggle>
        {
        }


        [SerializeField] List<ToggleExtended> m_Toggles;
        [SerializeField] private bool m_AllowMultiplySwitchOn = false;
        [SerializeField] private bool m_AllowSwitchOff = false;
        public bool allowSwitchOff { get { return m_AllowSwitchOff; } set { m_AllowSwitchOff = value; } }
        public bool allowMultiplySwitchOn { get { return m_AllowMultiplySwitchOn; } set { m_AllowMultiplySwitchOn = value; } }
        public ToggleEvent OnToggleOn;
        public Toggle[] Toggles { get => m_Toggles.ToArray(); }

        protected ToggleGroup()
        { }

        protected override void Awake()
        {
            base.Awake();
            foreach (var toggle in m_Toggles)
            {
                RegisterToggle(toggle);
                toggle.Init(this);
            }
        }

        private void ValidateToggleIsInGroup(Toggle toggle)
        {
            if (toggle == null || !m_Toggles.Contains(toggle))
                throw new ArgumentException(string.Format("Toggle {0} is not part of ToggleGroup {1}", new object[] { toggle, this }));
        }

        public void NotifyToggleOn(Toggle toggle)
        {
            ValidateToggleIsInGroup(toggle);

            OnToggleOn.Invoke(toggle);

            if (m_AllowMultiplySwitchOn)
                return;

            // disable all toggles in the group
            for (var i = 0; i < m_Toggles.Count; i++)
            {
                if (m_Toggles[i] == toggle)
                    continue;

                m_Toggles[i].isOn = false;
            }
        }

        public void UnregisterToggle(Toggle toggle)
        {
            toggle.onValueChanged.RemoveAllListeners();
        }

        public void RegisterToggle(Toggle toggle)
        {
            toggle.onValueChanged.AddListener(v => { if (v) NotifyToggleOn(toggle); });
        }

        public bool AnyTogglesOn()
        {
            return m_Toggles.Find(x => x.isOn) != null;
        }

        public IEnumerable<Toggle> ActiveToggles()
        {
            return m_Toggles.Where(x => x.isOn);
        }

        public IEnumerable<Toggle> InactiveToggles()
        {
            return m_Toggles.Where(x => !x.isOn);
        }

        public void SetAllTogglesOff()
        {
            bool oldAllowSwitchOff = m_AllowSwitchOff;
            m_AllowSwitchOff = true;

            for (var i = 0; i < m_Toggles.Count; i++)
                m_Toggles[i].isOn = false;

            m_AllowSwitchOff = oldAllowSwitchOff;
        }

        public void SetAllTogglesOn()
        {
            bool oldAllowMultiplySwitchOn = m_AllowMultiplySwitchOn;
            m_AllowMultiplySwitchOn = true;

            for (var i = 0; i < m_Toggles.Count; i++)
                m_Toggles[i].isOn = true;

            m_AllowMultiplySwitchOn = oldAllowMultiplySwitchOn;
        }
    }
}