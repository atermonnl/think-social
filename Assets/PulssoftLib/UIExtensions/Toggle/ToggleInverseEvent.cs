﻿using Pulssoft.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pulssoft.UIExtensions
{
    public class ToggleInverseEvent : MonoBehaviour
    {
        void Awake()
        {
            toggle.onValueChanged.AddListener(v => OnValueChangedInverted.Invoke(!v));
        }

        [SerializeField] Toggle toggle;
        public BoolEvent OnValueChangedInverted;
    }
}