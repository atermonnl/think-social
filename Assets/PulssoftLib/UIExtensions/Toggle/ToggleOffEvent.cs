﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Pulssoft.Utils;

namespace Pulssoft.UIExtensions
{
    public class ToggleOffEvent : MonoBehaviour
    {
        void Awake()
        {
            toggle.onValueChanged.AddListener(v => { if (!v) OnToggleIsOff.Invoke(); });
        }

        [SerializeField] Toggle toggle;
        public ActionEvent OnToggleIsOff;
    }
}