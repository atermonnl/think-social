﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Pulssoft.UIExtensions
{
    public class ToggleHighlighter : MonoBehaviour
    {
        [SerializeField]
        bool updateMaterialColor;

        [SerializeField]
        Graphic[] graphics;

        [SerializeField]
        Color highlightColor;

        [SerializeField]
        Color defColor;

        public bool highlighted;


        public void Highlight(bool value)
        {
            highlighted = value;
            foreach (var graphic in graphics)
            {
                graphic.color = value ? highlightColor : defColor;
                if (updateMaterialColor)
                    graphic.material.color = graphic.color;
            }
        }
    }
}
