﻿using UnityEditor;
using UnityEngine;

namespace Pulssoft
{
    public static class MenuExt
    {
        [MenuItem("Window/Open persistent data folder")]
        static void OpenPersistentDataPath()
        {
            EditorUtility.RevealInFinder(Application.persistentDataPath);
        }

        [MenuItem("Window/Clear player prefs")]
        static void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}