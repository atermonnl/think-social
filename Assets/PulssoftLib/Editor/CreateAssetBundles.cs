﻿using UnityEditor;
using UnityEngine;

namespace Pulssoft.Utils
{
    public class CreateAssetBundles
    {
        static string folder = "Assets/StreamingAssets/AssetBundles";

        [MenuItem("Assets/Build AssetBundles/Build Win64")]
        static void BuildAllAssetBundlesWin64()
        {
            BuildPipeline.BuildAssetBundles(folder, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
        }

        [MenuItem("Assets/Build AssetBundles/Build MacOS")]
        static void BuildAllAssetBundlesMacOS()
        {
            BuildPipeline.BuildAssetBundles(folder, BuildAssetBundleOptions.None, BuildTarget.StandaloneOSX);
        }

        [MenuItem("Assets/Build AssetBundles/Build iOS")]
        static void BuildAllAssetBundlesiOS()
        {
            BuildPipeline.BuildAssetBundles(folder, BuildAssetBundleOptions.None, BuildTarget.iOS);
        }

        [MenuItem("Assets/Build AssetBundles/Build Android")]
        static void BuildAllAssetBundlesAndroid()
        {
            BuildPipeline.BuildAssetBundles(folder, BuildAssetBundleOptions.None, BuildTarget.Android);
        }
    }
}