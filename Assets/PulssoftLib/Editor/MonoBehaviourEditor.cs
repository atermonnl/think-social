﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;
using System;
using Pulssoft.Utils;

namespace Pulssoft
{
    [CustomEditor(typeof(MonoBehaviour), true)]
    [CanEditMultipleObjects]
    public class MonoBehaviourEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (EditorUtility.IsDirty(target))
            {
                var monoBehaviour = (MonoBehaviour)target;
                Type type = monoBehaviour.GetType();
                do
                {
                    foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
                    {
                        if (field.IsPublic)
                        {
                            ValidateLink(field, monoBehaviour);
                        }

                        object[] attrs = field.GetCustomAttributes(true);
                        foreach (object attr in attrs)
                        {
                            try
                            {
                                if ((attr as RequiredAttribute) != null)
                                {
                                    var gc = monoBehaviour.gameObject.GetComponent(field.FieldType);
                                    if (!gc)
                                    {
                                        var value = field.GetValue(monoBehaviour);
                                        if (value == null || value.Equals(null))
                                        {
                                            field.SetValue(monoBehaviour, monoBehaviour.gameObject.AddComponent(field.FieldType));
                                        }
                                        else
                                        {
                                            Debug.LogErrorFormat("{0}: missing required component ({1}).", monoBehaviour.gameObject.name, field.FieldType);
                                        }
                                    }
                                    else
                                        field.SetValue(monoBehaviour, gc);
                                }


                                if ((attr as SerializeField) != null)
                                {
                                    ValidateLink(field, monoBehaviour);
                                }
                            }
                            catch (ArgumentException e)
                            {
                                Debug.LogError(e.Message);
                            }
                        }
                    }
                    type = type.BaseType;
                }
                while (type != typeof(MonoBehaviour));
            }
            base.OnInspectorGUI();
        }

        private void ValidateLink(FieldInfo field, MonoBehaviour monoBehaviour)
        {
            /*var value = field.GetValue(monoBehaviour) as Component;
            if (value != null)
            {
                if (!value.transform.IsChildOf(monoBehaviour.transform))
                {
                    field.SetValue(monoBehaviour, null);
                    Debug.LogError("Link validation error");
                }
            }*/
        }
    }
}